//
//  FontExtension.swift


import Foundation
import UIKit

extension UIFont {
    
    class func CalibriFont(with size:CGFloat) -> UIFont {
        return UIFont(name: "Calibri", size: size)!
    }
    
    //------------------------------------------------------------
    
    class func CalibriBOLDFont(with size:CGFloat) -> UIFont {
        return UIFont(name: "Calibri-Bold", size: size)!
    }
    
    //------------------------------------------------------------
    
    class func CalibriItalicFont(with size:CGFloat) -> UIFont {
        return UIFont(name: "Calibri-Italic", size: size)!
    }
    
    
    //------------------------------------------------------------
    
    class func CalibriBOLD_ItalicFont(with size:CGFloat) -> UIFont {
        return UIFont(name: "Calibri-BoldItalic", size: size)!
    }
    
    //------------------------------------------------------------

}
