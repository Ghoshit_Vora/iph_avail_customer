//
//  UIView+Borders.swift
//  UIViewWithSelectableBorders


import UIKit


extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
extension UITabBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 61 // adjust your size here
        return sizeThatFits
    }
}

// MARK: - UIView
public extension UISearchBar {
    
    public func setTextColor(color: UIColor) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.textColor = color
        let textFieldInsideSearchBarLabel = tf.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        let glassIconView = tf.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        glassIconView.tintColor = UIColor.white
        
        let clearButton = tf.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor.white
    }
}
extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var leftBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: newValue, height: bounds.height))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = UIColor(cgColor: layer.borderColor!)
           line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line(==lineWidth)]", options: [], metrics: metrics, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line]|", options: [], metrics: nil, views: views))
        }
    }
    
    @IBInspectable var topBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: bounds.width, height: newValue))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
           line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line]|", options: [], metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line(==lineWidth)]", options: [], metrics: metrics, views: views))
        }
    }
    
    @IBInspectable var rightBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: bounds.width, y: 0.0, width: newValue, height: bounds.height))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
           line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "[line(==lineWidth)]|", options: [], metrics: metrics, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line]|", options: [], metrics: nil, views: views))
        }
    }
    @IBInspectable var bottomBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: bounds.height, width: bounds.width, height: newValue))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
          line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line]|", options: [], metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[line(==lineWidth)]|", options: [], metrics: metrics, views: views))
        }
    }
     func removeborder() {
          for view in self.subviews {
               if view.tag == 110  {
                    view.removeFromSuperview()
               }
               
          }
     }
}

private var maxLengths = [UITextField: Int]()

extension UITextField {
    
    func setLeftPadding() {
        self.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    }
    
    @IBInspectable var maxLength: Int {
        
        get {
            
            guard let length = maxLengths[self]
                else {
                    return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            addTarget(
                self,
                action: #selector(limitLength),
                for: UIControlEvents.editingChanged
            )
        }
    }
    @objc func limitLength(textField: UITextField) {
        guard let prospectiveText = textField.text,
            prospectiveText.characters.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
}

extension UITableView
{
    func SetTableViewBlankLable(count:Int,str:String)
    {
        if(count==0)
        {
            removeOldLable()
            let lblText:UILabel = UILabel()
            lblText.frame = self.frame
            lblText.numberOfLines = 0
            lblText.text = str
            lblText.textAlignment = .center
            lblText.textColor = UIColor.lightGray
            self.backgroundColor = UIColor.clear
            self.separatorStyle = .none
            lblText.tag = 987654
            self.superview!.insertSubview(lblText, belowSubview: self)
        }
        else
        {
            removeOldLable()
            self.separatorStyle = .singleLine
        }
    }
    func removeOldLable()
    {
        for view in self.superview!.subviews
        {
            if(view is UILabel)
            {
                if(view.tag == 987654)
                {
                    view.removeFromSuperview()
                }
            }
        }
    }
}


extension UITextView {
    open override func awakeFromNib() {
        if DeviceType.IS_IPHONE_5 {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!-1)
        }
        if DeviceType.IS_IPHONE_6P {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!+3)
        }
        if DeviceType.IS_IPAD {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!+5)
        }
    }
}

extension UITextField {
    open override func awakeFromNib() {
        if DeviceType.IS_IPHONE_5 {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!-1)
        }
        if DeviceType.IS_IPHONE_6P {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!+3)
        }
        if DeviceType.IS_IPAD {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!+5)
        }
    }
}
extension UILabel {
    open override func awakeFromNib() {
        if DeviceType.IS_IPHONE_5 {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!-1)
        }
        if DeviceType.IS_IPHONE_6P {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!+3)
        }
        if DeviceType.IS_IPAD {
            self.font=UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!+5)
        }
    }
}
extension UIButton {
    open override func awakeFromNib() {
        if DeviceType.IS_IPHONE_5 {
            self.titleLabel?.font=UIFont(name: (self.titleLabel!.font?.fontName)!, size: (self.titleLabel!.font?.pointSize)!-1)
        }
        if DeviceType.IS_IPHONE_6P {
            self.titleLabel?.font=UIFont(name: (self.titleLabel!.font?.fontName)!, size: (self.titleLabel!.font?.pointSize)!+4)
        }
        else if DeviceType.IS_IPAD {
            self.titleLabel?.font=UIFont(name: (self.titleLabel!.font?.fontName)!, size: (self.titleLabel!.font?.pointSize)!+6)
        }
    }
    
    func applyShadow() {
        //self.layer.shadowColor = UIColor(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 0.5).cgColor
        //self.layer.shadowOffset = CGSize(width: 0, height: 1)
        //self.layer.shadowOpacity = 0.5
        //self.layer.masksToBounds = false
    }
}

extension UINavigationBar
{
    /// Applies a background gradient with the given colors
    func apply(gradient colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    /// Creates a gradient image with the given settings
    static func gradient(size : CGSize, colors : [UIColor]) -> UIImage?
    {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 1)
        
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: 0.0, y: size.height), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
