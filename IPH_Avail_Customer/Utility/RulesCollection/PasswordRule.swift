//
//  PasswordRule.swift


import Foundation

public enum PasswordPattern {

    case length
    case standard
    case regular

    public var pattern: String {
        switch self {
        case .length : return "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            //        case .regular: return "^(?=.*?[A-Z]).{6,}$"
        case .regular: return "^(?=.*?).{6,}$"
        case .standard: return "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,})$"
        }
    }
}

/**
 `PasswordRule` is a subclass of RegexRule that defines how a password is validated.
 */
open class PasswordRule: RegexRule {

    // Alternative Regexes

    // 8 characters. One uppercase. One Lowercase. One number.
    // static let regex = "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[a-z]).{8,}$"
    //
    // no length. One uppercase. One lowercae. One number.
    // static let regex = "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[a-z]).*?$"

    /**
     Initializes a `PasswordRule` object that will validate a field is a valid password.

     - parameter message: String of error message.
     - returns: An initialized `PasswordRule` object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public convenience init(rulePattern: PasswordPattern = .regular, message: String = ErrorManager.passwordError.errorDescription!) {
        self.init(regex: rulePattern.pattern, message: message)
    }
}
