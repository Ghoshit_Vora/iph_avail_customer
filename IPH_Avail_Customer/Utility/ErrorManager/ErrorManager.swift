//
//  ErrorManager.swift


import Foundation

internal extension String {

}

// Error Manager
/// This Enum contains different cases for the Error for the Application
///
/// - noInternetConnectionError: For NoInternetConnection
/// - requiredFieldError: For Required Field Error
/// - compareTextFieldError: For Compare Text Field Error
/// - characterSetError: For Character Set Error
/// - alphabeticError: For Alphabetic Error
/// - alphaNumbericError: For alphaNumbericError
/// - phoneNumberError: For Phone number Error
/// - regexError: Regex Error
/// - exactLengthError: Same length Error
/// - maxLengthError: Max Length Error
/// - minLengthError: Min Length Error
/// - zipCodeError: Zip Code Error
/// - passwordError: Password Error
/// - emailError: Email Error
/// - numberError: Numberic Error
/// - upperCaseError: Upper Case Error
/// - lowerCaseError: Lower Case Error
/// - fullNameError: Fullname Error
/// - locationWhenInUseUsageDescription: Error for not finding key in the plist
/// - locationAlwaysUsageDescription: Error for not finding key in the plist
/// - microphoneUsageDescription: Error for not finding key in the plist
/// - speechRecognitionUsageDescription: Error for not finding key in the plist
/// - photoLibraryUsageDescription: Error for not finding key in the plist
/// - cameraUsageDescription: Error for not finding key in the plist
/// - mediaLibraryUsageDescription: Error for not finding key in the plist
/// - siriUsageDescription: Error for not finding key in the plist
/// - contactsUsageDescription: Error for not finding key in the plist
/// - calendarsUsageDescription: Error for not finding key in the plist
/// - remindersUsageDescription: Error for not finding key in the plist
/// - bluetoothPeripheralUsageDescription: Error for not finding key in the plist
public enum ErrorManager: Error {

    case noInternetConnectionError

    case requiredFieldError

    case compareTextFieldError

    case characterSetError

    case alphabeticError

    case alphaNumbericError

    case phoneNumberError

    case regexError

    case exactLengthError

    case maxLengthError

    case minLengthError

    case zipCodeError

    case passwordError

    case emailError

    case numberError

    case upperCaseError

    case lowerCaseError

    case fullNameError

    case locationWhenInUseUsageDescription

    case locationAlwaysUsageDescription

    case microphoneUsageDescription

    case speechRecognitionUsageDescription

    case photoLibraryUsageDescription

    case cameraUsageDescription

    case mediaLibraryUsageDescription

    case siriUsageDescription

    case contactsUsageDescription

    case calendarsUsageDescription

    case remindersUsageDescription

    case bluetoothPeripheralUsageDescription

}

// MARK: - Added Localized Description in the ErrorManager class for all the Cases
extension ErrorManager {

    var localizedDescription: String {

        switch self {

        case .noInternetConnectionError:
            return "No Internet Connection found"

        case .requiredFieldError:
            return "All Field are required"

        case .compareTextFieldError:
            return ""

        case .characterSetError:
            return "Text Mismatch"

        case .alphabeticError:
            return "Enter valid alphabetic characters"

        case .alphaNumbericError:
            return "Enter valid alpha numeric characters"

        case .phoneNumberError:
            return "Enter a valid 10 digit phone number"

        case .regexError:
            return "Invalid Regular Expression"

        case .exactLengthError:
            return "Must be at most 16 characters long"

        case .minLengthError:
            return "Must be at least 3 characters long"

        case .maxLengthError:
            return "Must be at most 16 characters long"

        case .zipCodeError:
            return "Enter a valid 5 digit zipcode"

        case .passwordError:
            return "Must be 8 characters with 1 uppercase"

        case .emailError:
            return "Must be a valid email address"

        case .numberError:
            return "Enter valid numeric characters"

        case .upperCaseError:
            return "All the Character must be in the Upper Case"

        case .lowerCaseError:
            return "All the Character must be in the Lower Case"

        case .fullNameError:
            return "Please provide a first & last name"

        case .locationWhenInUseUsageDescription:
            return "NSLocationWhenInUseUsageDescription not found in Info.plist"

        case .locationAlwaysUsageDescription:
            return "NSLocationAlwaysUsageDescription not found in Info.plist"

        case .microphoneUsageDescription:
            return "NSMicrophoneUsageDescription not found in Info.plist"

        case .speechRecognitionUsageDescription:
            return "NSSpeechRecognitionUsageDescription not found in Info.plist"

        case .photoLibraryUsageDescription:
            return "NSPhotoLibraryUsageDescription not found in Info.plist"

        case .cameraUsageDescription:
            return "NSCameraUsageDescription not found in Info.plist"

        case .mediaLibraryUsageDescription:
            return "NSAppleMusicUsageDescription not found in Info.plist"

        case .siriUsageDescription:
            return "NSSiriUsageDescription not found in Info.plist"

        case .contactsUsageDescription:
            return "NSContactsUsageDescription not found in Info.plist"

        case .calendarsUsageDescription:
            return "NSCalendarsUsageDescription not found in Info.plist"

        case .remindersUsageDescription:
            return "NSRemindersUsageDescription not found in Info.plist"

        case .bluetoothPeripheralUsageDescription:
            return "NSBluetoothPeripheralUsageDescription not found in Info.plist"

        }

    }
}
// MARK: - Localized Error
extension ErrorManager: LocalizedError {

    public var errorDescription: String? {

        switch self {

        case .noInternetConnectionError:
            return "No Internet Connection found"

        case .requiredFieldError:
            return "All Field are required"

        case .compareTextFieldError:
            return ""

        case .characterSetError:
            return "Please enter characters only"

        case .alphabeticError:
            return "Enter valid alphabetic characters"

        case .alphaNumbericError:
            return "Enter valid alpha numeric characters"

        case .phoneNumberError:
            return "Enter a valid 10 digit phone number"

        case .regexError:
            return "Invalid Regular Expression"

        case .exactLengthError:
            return "Must be at most 16 characters long"

        case .maxLengthError:
            return "Must be at most 16 characters long"

        case .minLengthError:
            return "Must be at least 3 characters long"

        case .zipCodeError:
            return "Enter a valid 5 digit zipcode"

        case .passwordError:
            return "Must be 8 characters with 1 uppercase"

        case .emailError:
            return "Must be a valid email address"

        case .numberError:
            return "Enter valid numeric characters"

        case .upperCaseError:
            return "All the Character must be in the Upper Case"

        case .lowerCaseError:
            return "All the Character must be in the Lower Case"

        case .fullNameError:
            return "Please provide a first & last name"

        case .locationWhenInUseUsageDescription:
            return "NSLocationWhenInUseUsageDescription"

        case .locationAlwaysUsageDescription:
            return "NSLocationAlwaysUsageDescription"

        case .microphoneUsageDescription:
            return "NSMicrophoneUsageDescription"

        case .speechRecognitionUsageDescription:
            return "NSSpeechRecognitionUsageDescription"

        case .photoLibraryUsageDescription:
            return "NSPhotoLibraryUsageDescription"

        case .cameraUsageDescription:
            return "NSCameraUsageDescription"

        case .mediaLibraryUsageDescription:
            return "NSAppleMusicUsageDescription"

        case .siriUsageDescription:
            return "NSSiriUsageDescription"

        case .contactsUsageDescription:
            return "NSContactsUsageDescription"

        case .calendarsUsageDescription:
            return "NSCalendarsUsageDescription"

        case .remindersUsageDescription:
            return "NSRemindersUsageDescription"

        case .bluetoothPeripheralUsageDescription:
            return "NSBluetoothPeripheralUsageDescription"

        }
    }
}
