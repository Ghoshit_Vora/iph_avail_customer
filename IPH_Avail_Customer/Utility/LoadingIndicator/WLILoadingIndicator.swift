
import Foundation
import UIKit

/// Type of all the indicator supported by the WLILoadingIndicator Class
///
/// - ballSpinFadeLoader: Ball Spin Fade Loader
/// - spinBallPulse: Splin Ball Loader
/// - ballClipRotate: Ball Clip Loader
/// - nativeSquare: Native Square Loader
/// - lineScale: Line Scale Loader
/// - speedFadeLoader: Speed Fade Loader
/// - ballRippleMultiple: Ball Rippple Loader
/// - roundTriangle: Round Triangle Loader
/// - squareSpin: Sqaure Spin Loader
/// - audioEqualizer: Audio Equalizer Loader
/// - semiCircleSpin: Semi Circle Spin Loader
/// - ballRotateChase: Ball Rotate Loader
/// - roundPulseSync: Round Pulse Loader
/// - ballGridPulse: Ball Grid Loader
/// - ballPulseRise: Ball Pulse Loader
/// - ballSpinRotate: Ball Spin Rotate Loader
/// - ballScaleMultiple: Ball Scale Multiple
/// - customRound: Custom Round Loader
/// - customOrbit: Custom Order Loader
/// - triagleSkewSpin: Triange Skew Spin Loader
/// - lineScalePulseOutRapid: Line Scale Pulse Loader

public enum IndicatorType {
    case ballSpinFadeLoader
    case spinBallPulse
    case ballClipRotate
    case nativeSquare
    case lineScale
    case speedFadeLoader
    case ballRippleMultiple
    case roundTriangle
    case squareSpin
    case audioEqualizer
    case semiCircleSpin
    case ballRotateChase
    case roundPulseSync
    case ballGridPulse
    case ballPulseRise
    case ballSpinRotate
    case ballScaleMultiple
    case customRound
    case customOrbit
    case triagleSkewSpin
    case lineScalePulseOutRapid
}

// -----------------------------------------------------------------------

/// Animation Type for the Loader while presenting
///
/// - circle: Circle Effect for the Loader
/// - circleSemi: Semi Circle Effect for the loader
/// - ring: Ring Effecet for the Loader
/// - ringTwoHalfVertical: Ring two Half Vertical Effect
/// - ringTwoHalfHorizontal: Ring two Half Horizontal Effect
/// - ringThirdFour: Ring Third Four Effect
/// - rectangle: Rectangle LOading Effect
/// - triangle: Triangle Loading Effect
/// - line: Line Loading Effect
/// - pacman: Pacman game effect
private enum AnimationSubType {
    case circle
    case circleSemi
    case ring
    case ringTwoHalfVertical
    case ringTwoHalfHorizontal
    case ringThirdFour
    case rectangle
    case triangle
    case line
    case pacman
}

/// WLILoading Indicator
class WLILoadingIndicator: NSObject {
    
    static let sharedManager = WLILoadingIndicator()
    fileprivate let restorationIdentifier = "WLIIndicatorViewContainer"
    fileprivate var showTimer: Timer?
    fileprivate var hideTimer: Timer?
    fileprivate var isStopAnimatingCalled = false
    
    // -----------------------------------------------------------------------
    
    fileprivate override init() {}
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Start Loading Indicator
    
    // -----------------------------------------------------------------------
    
    public final func startAnimating(_ data: WLILoadingIndicatorProperties) {
        guard showTimer == nil else {
            return
        }
        isStopAnimatingCalled = false
        showTimer = scheduledTimer(data.displayTimeThreshold, selector: #selector(showTimerFired(_:)), data: data)
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Stop Loading Indicator
    
    // -----------------------------------------------------------------------
    
    public final func stopAnimating() {
        isStopAnimatingCalled = true
        guard hideTimer == nil else {
            return
        }
        stopLoadingIndicator()
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Start-Stop Timer Methods
    
    // -----------------------------------------------------------------------
    
    @objc fileprivate func showTimerFired(_ timer: Timer) {
        guard let activityData = timer.userInfo as? WLILoadingIndicatorProperties else { return }
        if activityData.indicatorType == .nativeSquare {
            setupDataForBasicIndicator(activityData)
        } else {
            startLoadingIndicator(with: activityData)
        }
    }
    
    // -----------------------------------------------------------------------
    
    @objc fileprivate func hideTimerFired(_ timer: Timer) {
        hideTimer?.invalidate()
        hideTimer = nil
        if isStopAnimatingCalled {
            stopLoadingIndicator()
        }
    }
    
    // -----------------------------------------------------------------------
    
    fileprivate func scheduledTimer(_ timeInterval: Int, selector: Selector, data: WLILoadingIndicatorProperties?) -> Timer {
        return Timer.scheduledTimer(timeInterval: Double(timeInterval) / 1000,
                                    target: self,
                                    selector: selector,
                                    userInfo: data,
                                    repeats: false)
    }
    
    // -----------------------------------------------------------------------
    
    fileprivate func startLoadingIndicator(with activityData: WLILoadingIndicatorProperties) {
        let activityContainer: UIView = UIView(frame: UIScreen.main.bounds)
        activityContainer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        activityContainer.restorationIdentifier = restorationIdentifier
        
        hideTimer = scheduledTimer(activityData.minimumDisplayTime, selector: #selector(hideTimerFired(_:)), data: nil)
        
        guard UIApplication.shared.keyWindow != nil else {
            return
        }
        addConstraints(to: activityContainer, activityData: activityData)
        
    }
    
    // -----------------------------------------------------------------------
    
    fileprivate func stopLoadingIndicator() {
        guard let keyWindow = UIApplication.shared.keyWindow else {
            return
        }
        
        for item in (keyWindow.subviews)
            where item.restorationIdentifier == restorationIdentifier {
                item.removeFromSuperview()
        }
        showTimer?.invalidate()
        showTimer = nil
    }
    
    // -----------------------------------------------------------------------
    
    open func setupDataForBasicIndicator(_ activityData: WLILoadingIndicatorProperties) {
        let actualSize = activityData.size
        
        let viewContent = UIView()
        viewContent.translatesAutoresizingMaskIntoConstraints = false
        viewContent.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        viewContent.restorationIdentifier = restorationIdentifier
        //        if let window = UIApplication.shared.keyWindow, let view = window.rootViewController?.view {
        if let view = UIApplication.shared.keyWindow {
            viewContent.frame = view.frame
            //            let count = view.subviews.count
            view.addSubview(viewContent)
            viewContent.translatesAutoresizingMaskIntoConstraints = false
            
            view.addConstraint(NSLayoutConstraint.init(item: viewContent, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0))
            
            view.addConstraint(NSLayoutConstraint.init(item: viewContent, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0))
            
            view.addConstraint(NSLayoutConstraint.init(item: viewContent, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 0))
            
            view.addConstraint(NSLayoutConstraint.init(item: viewContent, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: 0))
            let viewIndicator = UIView()
            viewIndicator.backgroundColor = activityData.indicatorColor
            viewIndicator.layer.cornerRadius = 10
            viewIndicator.layer.masksToBounds = true
            viewIndicator.translatesAutoresizingMaskIntoConstraints = false
            viewContent.addSubview(viewIndicator)
            
            viewContent.addConstraint(NSLayoutConstraint.init(item: viewIndicator, attribute: .centerX, relatedBy: .equal, toItem: viewContent, attribute: .centerX, multiplier: 1.0, constant: 0))
            
            viewContent.addConstraint(NSLayoutConstraint.init(item: viewIndicator, attribute: .centerY, relatedBy: .equal, toItem: viewContent, attribute: .centerY, multiplier: 1.0, constant: -15))
            
            viewIndicator.addConstraint(NSLayoutConstraint.init(item: viewIndicator, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: actualSize.height))
            
            viewIndicator.addConstraint(NSLayoutConstraint.init(item: viewIndicator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: actualSize.width))
            
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.color = UIColor.black
            viewIndicator.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            viewIndicator.addConstraint(NSLayoutConstraint.init(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: viewIndicator, attribute: .centerX, multiplier: 1.0, constant: 0))
            
            viewIndicator.addConstraint(NSLayoutConstraint.init(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: viewIndicator, attribute: .centerY, multiplier: 1.0, constant: 0))
            
            if let message = activityData.message, !message.isEmpty {
                let label = UILabel()
                
                label.textAlignment = .center
                label.text = message
                label.font = activityData.messageFont
                label.textColor = activityData.textColor
                label.numberOfLines = 0
                label.lineBreakMode = .byWordWrapping
                label.sizeToFit()
                if label.bounds.size.width > viewContent.bounds.size.width {
                    let maxWidth = viewContent.bounds.size.width - 16
                    
                    label.bounds.size = NSString(string: message).boundingRect(with: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: label.font], context: nil).size
                }
                
                label.translatesAutoresizingMaskIntoConstraints = false
                viewContent.addSubview(label)
                
                viewContent.addConstraint(NSLayoutConstraint.init(item: label, attribute: .top, relatedBy: .equal, toItem: viewIndicator, attribute: .bottom, multiplier: 1.0, constant: 10))
                
                viewContent.addConstraint(NSLayoutConstraint.init(item: label, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1.0, constant: 10))
                
                viewContent.addConstraint(NSLayoutConstraint.init(item: label, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1.0, constant: -10))
            }
            
        } else {
            print("No root view found.")
        }
        
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Add Constraints to the Content
    
    // -----------------------------------------------------------------------
    
    func addConstraints(to viewContent: UIView, activityData: WLILoadingIndicatorProperties) {
        
        if let view = UIApplication.shared.keyWindow {
            
            viewContent.frame = view.frame
            //            let count = view.subviews.count
            view.addSubview(viewContent)
            viewContent.translatesAutoresizingMaskIntoConstraints = false
            
            view.addConstraint(NSLayoutConstraint.init(item: viewContent, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0))
            
            view.addConstraint(NSLayoutConstraint.init(item: viewContent, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0))
            
            view.addConstraint(NSLayoutConstraint.init(item: viewContent, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 0))
            
            view.addConstraint(NSLayoutConstraint.init(item: viewContent, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: 0))
            
            let actualSize = activityData.size
            let activityIndicatorView = IndicatorView(frame: CGRect(x: 0, y: 0, width: actualSize.width, height: actualSize.height), indicatorType: activityData.indicatorType, color: activityData.indicatorColor, padding: activityData.padding)
            activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
            //            activityIndicatorView.center = viewContent.center
            activityIndicatorView.startAnimating(with: activityData.indicatorType!)
            viewContent.addSubview(activityIndicatorView)
            
            viewContent.addConstraint(NSLayoutConstraint.init(item: activityIndicatorView, attribute: .centerX, relatedBy: .equal, toItem: viewContent, attribute: .centerX, multiplier: 1.0, constant: 0))
            
            viewContent.addConstraint(NSLayoutConstraint.init(item: activityIndicatorView, attribute: .centerY, relatedBy: .equal, toItem: viewContent, attribute: .centerY, multiplier: 1.0, constant: -15))
            
            activityIndicatorView.addConstraint(NSLayoutConstraint.init(item: activityIndicatorView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: actualSize.height))
            
            activityIndicatorView.addConstraint(NSLayoutConstraint.init(item: activityIndicatorView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: actualSize.width))
            
            if let message = activityData.message, !message.isEmpty {
                let label = UILabel()
                
                label.textAlignment = .center
                label.text = message
                label.font = activityData.messageFont
                label.textColor = activityData.textColor
                label.numberOfLines = 0
                label.lineBreakMode = .byWordWrapping
                label.sizeToFit()
                if label.bounds.size.width > viewContent.bounds.size.width {
                    let maxWidth = viewContent.bounds.size.width - 16
                    
                    label.bounds.size = NSString(string: message).boundingRect(with: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: label.font], context: nil).size
                }
                //                label.center = CGPoint(
                //                    x: activityIndicatorView.center.x,
                //                    y: activityIndicatorView.center.y + actualSize.height + label.bounds.size.height / 2 + 8)
                label.translatesAutoresizingMaskIntoConstraints = false
                viewContent.addSubview(label)
                
                viewContent.addConstraint(NSLayoutConstraint.init(item: label, attribute: .top, relatedBy: .equal, toItem: activityIndicatorView, attribute: .bottom, multiplier: 1.0, constant: 10))
                
                viewContent.addConstraint(NSLayoutConstraint.init(item: label, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1.0, constant: 10))
                
                viewContent.addConstraint(NSLayoutConstraint.init(item: label, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1.0, constant: -10))
            }
            
        } else {
            print("No root view found.")
        }
    }
    
    // -----------------------------------------------------------------------
}

// -----------------------------------------------------------------------

open class WLILoadingIndicatorProperties: NSObject {
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Properties
    
    // -----------------------------------------------------------------------
    
    let size: CGSize
    let message: String?
    let messageFont: UIFont
    let indicatorType: IndicatorType?
    let indicatorColor: UIColor
    let textColor: UIColor
    let padding: CGFloat
    let displayTimeThreshold: Int
    let minimumDisplayTime: Int
    
    // -----------------------------------------------------------------------
    
    public init(size: CGSize = CGSize(width: 60, height: 60), message: String, messageFont: UIFont = UIFont.boldSystemFont(ofSize: 18), type: IndicatorType, indicatorColor: UIColor, textColor: UIColor) {
        self.size = size
        self.message = message
        self.messageFont = messageFont
        self.indicatorType = type
        self.indicatorColor = indicatorColor
        self.textColor = textColor
        self.padding = IndicatorView.DefaultParameters.padding
        self.displayTimeThreshold = IndicatorView.DefaultParameters.displayTime
        self.minimumDisplayTime = IndicatorView.DefaultParameters.minimumTime
    }
}

// -----------------------------------------------------------------------

private final class IndicatorView: UIView {
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Default Loading Indicator Properties
    
    // -----------------------------------------------------------------------
    
    struct DefaultParameters {
        static var indicatorColor = UIColor.white
        static var textColor = UIColor.white
        static var padding: CGFloat = 0
        static var blockerSize = CGSize(width: 60, height: 60)
        static var displayTime = 0
        static var minimumTime = 0
        static var blockerMessage: String? = nil
        static var blockerMessageFont = UIFont.boldSystemFont(ofSize: 18)
    }
    
    public var indicatorType: IndicatorType?
    
    @IBInspectable public var color: UIColor = IndicatorView.DefaultParameters.indicatorColor
    
    @IBInspectable public var padding: CGFloat = IndicatorView.DefaultParameters.padding
    
    @available(*, deprecated: 3.1)
    
    public var animating: Bool { return isAnimating }
    
    fileprivate(set) public var isAnimating: Bool = false
    
    // -----------------------------------------------------------------------
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
        isHidden = true
    }
    
    // -----------------------------------------------------------------------
    
    public init(frame: CGRect, indicatorType: IndicatorType? = nil, color: UIColor? = nil, padding: CGFloat? = nil) {
        self.indicatorType = indicatorType ?? .squareSpin
        self.color = color ?? IndicatorView.DefaultParameters.indicatorColor
        self.padding = padding ?? IndicatorView.DefaultParameters.padding
        super.init(frame: frame)
        isHidden = true
    }
    
    // -----------------------------------------------------------------------
    
    public override var intrinsicContentSize: CGSize {
        return CGSize(width: bounds.width, height: bounds.height)
    }
    
    // -----------------------------------------------------------------------
    
    public final func startAnimating(with Type: IndicatorType) {
        isHidden = false
        isAnimating = true
        layer.speed = 1
        setUpAnimation(with: Type)
    }
    
    // -----------------------------------------------------------------------
    
    public final func stopAnimating() {
        isHidden = true
        isAnimating = false
        layer.sublayers?.removeAll()
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Animation related Methods
    
    // -----------------------------------------------------------------------
    
    fileprivate final func setUpAnimation(with type: IndicatorType) {
        var animationRect = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(padding, padding, padding, padding))
        let minEdge = min(animationRect.width, animationRect.height)
        
        layer.sublayers = nil
        animationRect.size = CGSize(width: minEdge, height: minEdge)
        switch type {
        case .squareSpin:
            setUpAnimationSquareSpin(in: layer, size: animationRect.size, color: color)
        case .ballSpinFadeLoader:
            setUpAnimationBallSpinFadeLoader(in: layer, size: animationRect.size, color: color)
        case .ballClipRotate:
            setUpAnimationBallClipRotate(in: layer, size: animationRect.size, color: color)
        case .lineScale:
            setUpAnimationLineScale(in: layer, size: animationRect.size, color: color)
        case .speedFadeLoader:
            setUpAnimationSpeedFadeLoader(in: layer, size: animationRect.size, color: color)
        case .ballRippleMultiple:
            setUpAnimationBallRippleMultiple(in: layer, size: animationRect.size, color: color)
        case .roundTriangle:
            setUpAnimationRoundTriangle(in: layer, size: animationRect.size, color: color)
        case .audioEqualizer:
            setUpAnimationAudioEqaulizer(in: layer, size: animationRect.size, color: color)
        case .semiCircleSpin:
            setUpAnimationSemiCircleSpin(in: layer, size: animationRect.size, color: color)
        case .spinBallPulse:
            setUpAnimationSpinBallPulse(in: layer, size: animationRect.size, color: color)
        case .ballRotateChase:
            setUpAnimationBallRotateChase(in: layer, size: animationRect.size, color: color)
        case .roundPulseSync:
            setUpAnimationRoundPulseSync(in: layer, size: animationRect.size, color: color)
        case .nativeSquare:
            break
        case .ballGridPulse:
            setUpAnimationBallGridPulse(in: layer, size: animationRect.size, color: color)
        case .ballPulseRise:
            setUpAnimationBallPulseRise(in: layer, size: animationRect.size, color: color)
        case .ballSpinRotate:
            setUpAnimationBallSpinRotate(in: layer, size: animationRect.size, color: color)
        case .ballScaleMultiple:
            setUpAnimationBallScaleMultiple(in: layer, size: animationRect.size, color: color)
        case .customRound:
            setUpAnimationCustomRound(in: layer, size: animationRect.size, color: color)
        case .customOrbit:
            setUpAnimationCustomOrbit(in: layer, size: animationRect.size, color: color)
        case .triagleSkewSpin:
            setUpAnimationTriangleSkewSpin(in: layer, size: animationRect.size, color: color)
        case .lineScalePulseOutRapid:
            setUpAnimationLineScalePulseOutRapid(in: layer, size: animationRect.size, color: color)
        }
        
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - SquareSpin Loading Indicator Animation
    
    // -----------------------------------------------------------------------
    
    fileprivate func setUpAnimationSquareSpin(in layer: CALayer, size: CGSize, color: UIColor) {
        let duration: CFTimeInterval = 3
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.09, 0.57, 0.49, 0.9)
        
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "transform")
        
        animation.keyTimes = [0, 0.25, 0.5, 0.75, 1]
        animation.timingFunctions = [timingFunction, timingFunction, timingFunction, timingFunction]
        animation.values = [
            NSValue(caTransform3D: CATransform3DConcat(createRotateXTransform(0), createRotateYTransform(0))),
            NSValue(caTransform3D: CATransform3DConcat(createRotateXTransform(CGFloat(Double.pi)), createRotateYTransform(0))),
            NSValue(caTransform3D: CATransform3DConcat(createRotateXTransform(CGFloat(Double.pi)), createRotateYTransform(CGFloat(Double.pi)))),
            NSValue(caTransform3D: CATransform3DConcat(createRotateXTransform(0), createRotateYTransform(CGFloat(Double.pi)))),
            NSValue(caTransform3D: CATransform3DConcat(createRotateXTransform(0), createRotateYTransform(0)))]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        let square = layerWith(size, color: color, animType: .rectangle)
        
        let frame = CGRect(x: (layer.bounds.size.width - size.width) / 2,
                           y: (layer.bounds.size.height - size.height) / 2,
                           width: size.width,
                           height: size.height)
        
        square.frame = frame
        square.add(animation, forKey: "animation")
        layer.addSublayer(square)
    }
    
    // -----------------------------------------------------------------------
    
    fileprivate func createRotateXTransform(_ angle: CGFloat) -> CATransform3D {
        var transform = CATransform3DMakeRotation(angle, 1, 0, 0)
        
        transform.m34 = CGFloat(-1) / 100
        
        return transform
    }
    
    // -----------------------------------------------------------------------
    
    fileprivate func createRotateYTransform(_ angle: CGFloat) -> CATransform3D {
        var transform = CATransform3DMakeRotation(angle, 0, 1, 0)
        
        transform.m34 = CGFloat(-1) / 100
        
        return transform
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - BallSpin Fade Loading Indicator Animation
    
    // -----------------------------------------------------------------------
    
    fileprivate func setUpAnimationBallSpinFadeLoader(in layer: CALayer, size: CGSize, color: UIColor) {
        let circleSpacing: CGFloat = -2
        let circleSize = (size.width - 4 * circleSpacing) / 5
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - size.height) / 2
        let duration: CFTimeInterval = 1
        let beginTime = CACurrentMediaTime()
        let beginTimes: [CFTimeInterval] = [0, 0.12, 0.24, 0.36, 0.48, 0.6, 0.72, 0.84]
        
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.5, 1]
        scaleAnimation.values = [1, 0.4, 1]
        scaleAnimation.duration = duration
        
        // Opacity animation
        let opacityAnimaton = CAKeyframeAnimation(keyPath: "opacity")
        
        opacityAnimaton.keyTimes = [0, 0.5, 1]
        opacityAnimaton.values = [1, 0.3, 1]
        opacityAnimaton.duration = duration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, opacityAnimaton]
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circles
        for i in 0 ..< 8 {
            let circle = circleAt(CGFloat((Double.pi / 4) * Double(i)),
                                  size: circleSize,
                                  origin: CGPoint(x: x, y: y),
                                  containerSize: size,
                                  color: color)
            
            animation.beginTime = beginTime + beginTimes[i]
            circle.add(animation, forKey: "animation")
            layer.addSublayer(circle)
        }
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - BallClipRotate Loading Indicator Animation
    
    // -----------------------------------------------------------------------
    
    fileprivate func setUpAnimationBallClipRotate(in layer: CALayer, size: CGSize, color: UIColor) {
        let duration: CFTimeInterval = 1.50
        
        //    Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.5, 1]
        scaleAnimation.values = [1, 0.6, 1]
        
        // Rotate animation
        let rotateAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        
        rotateAnimation.keyTimes = scaleAnimation.keyTimes
        rotateAnimation.values = [0, Double.pi, 2 * Double.pi]
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, rotateAnimation]
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        
        let circle = layerWith(CGSize(width: size.width - 20, height: size.height - 20), color: color, animType: .ringThirdFour)
        
        let frame = CGRect(x: ((layer.bounds.size.width - size.width) / 2) + 10,
                           y: ((layer.bounds.size.height - size.height) / 2) + 10,
                           width: size.width - 20,
                           height: size.height - 20)
        
        circle.frame = frame
        layer.frame.size.width = 100
        layer.frame.size.height = 100
        layer.backgroundColor = UIColor.black.cgColor
        layer.cornerRadius = 10
        layer.masksToBounds = true
        circle.add(animation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    
    fileprivate func circleAt(_ angle: CGFloat, size: CGFloat, origin: CGPoint, containerSize: CGSize, color: UIColor) -> CALayer {
        let radius = containerSize.width / 2 - size / 2
        
        let circle = layerWith(CGSize(width: size, height: size), color: color, animType: .circle)
        
        let frame = CGRect(
            x: origin.x + radius * (cos(angle) + 1),
            y: origin.y + radius * (sin(angle) + 1),
            width: size,
            height: size)
        
        circle.frame = frame
        
        return circle
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - LineScale Loading Indicator Animation
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationLineScale(in layer: CALayer, size: CGSize, color: UIColor) {
        let lineSize = size.width / 9
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - size.height) / 2
        let duration: CFTimeInterval = 1
        let beginTime = CACurrentMediaTime()
        let beginTimes = [0.1, 0.2, 0.3, 0.4, 0.5]
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.2, 0.68, 0.18, 1.08)
        
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        
        animation.keyTimes = [0, 0.5, 1]
        animation.timingFunctions = [timingFunction, timingFunction]
        animation.values = [1, 0.4, 1]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        for i in 0 ..< 5 {
            let line = layerWith(CGSize(width: lineSize, height: size.height), color: color, animType: .line)
            
            let frame = CGRect(x: x + lineSize * 2 * CGFloat(i), y: y, width: lineSize, height: size.height)
            
            animation.beginTime = beginTime + beginTimes[i]
            line.frame = frame
            line.add(animation, forKey: "animation")
            layer.addSublayer(line)
        }
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - SpeedFadeLoader Loading Indicator Animation
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationSpeedFadeLoader(in layer: CALayer, size: CGSize, color: UIColor) {
        let lineSpacing: CGFloat = 2
        let lineSize = CGSize(width: (size.width - 4 * lineSpacing) / 5, height: (size.height - 2 * lineSpacing) / 3)
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - size.height) / 2
        let duration: CFTimeInterval = 1.2
        let beginTime = CACurrentMediaTime()
        let beginTimes: [CFTimeInterval] = [0.12, 0.24, 0.36, 0.48, 0.6, 0.72, 0.84, 0.96]
        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "opacity")
        
        animation.keyTimes = [0, 0.5, 1]
        animation.timingFunctions = [timingFunction, timingFunction]
        animation.values = [1, 0.3, 1]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw lines
        for i in 0 ..< 8 {
            let line = lineAt(CGFloat((Double.pi / 4) * Double(i)),
                              size: lineSize,
                              origin: CGPoint(x: x, y: y),
                              containerSize: size,
                              color: color)
            
            animation.beginTime = beginTime + beginTimes[i]
            line.add(animation, forKey: "animation")
            layer.addSublayer(line)
        }
    }
    
    // -----------------------------------------------------------------------
    
    func lineAt(_ angle: CGFloat, size: CGSize, origin: CGPoint, containerSize: CGSize, color: UIColor) -> CALayer {
        let radius = containerSize.width / 2 - max(size.width, size.height) / 2
        let lineContainerSize = CGSize(width: max(size.width, size.height), height: max(size.width, size.height))
        let lineContainer = CALayer()
        let lineContainerFrame = CGRect(
            x: origin.x + radius * (cos(angle) + 1),
            y: origin.y + radius * (sin(angle) + 1),
            width: lineContainerSize.width,
            height: lineContainerSize.height)
        let line = layerWith(size, color: color, animType: .line)
        
        let lineFrame = CGRect(
            x: (lineContainerSize.width - size.width) / 2,
            y: (lineContainerSize.height - size.height) / 2,
            width: size.width,
            height: size.height)
        
        lineContainer.frame = lineContainerFrame
        line.frame = lineFrame
        lineContainer.addSublayer(line)
        lineContainer.sublayerTransform = CATransform3DMakeRotation(CGFloat(Double.pi) + angle, 0, 0, 1)
        
        return lineContainer
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - BallRippleMultiple Loading Indicator methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationBallRippleMultiple(in layer: CALayer, size: CGSize, color: UIColor) {
        let duration: CFTimeInterval = 1.25
        let beginTime = CACurrentMediaTime()
        let beginTimes = [0.0, 0.2, 0.4]
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.21, 0.53, 0.56, 0.8)
        
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.7]
        scaleAnimation.timingFunction = timingFunction
        scaleAnimation.values = [0, 1.0]
        scaleAnimation.duration = duration
        
        // Opacity animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        
        opacityAnimation.keyTimes = [0, 0.7, 1]
        opacityAnimation.timingFunctions = [timingFunction, timingFunction]
        opacityAnimation.values = [1, 0.7, 0]
        opacityAnimation.duration = duration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circles
        for i in 0 ..< 3 {
            let circle = layerWith(size, color: color, animType: .ring)
            
            let frame = CGRect(x: (layer.bounds.size.width - size.width) / 2,
                               y: (layer.bounds.size.height - size.height) / 2,
                               width: size.width,
                               height: size.height)
            
            animation.beginTime = beginTime + beginTimes[i]
            circle.frame = frame
            circle.add(animation, forKey: "animation")
            layer.addSublayer(circle)
        }
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - RoundTriangle Loading Indicator methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationRoundTriangle(in layer: CALayer, size: CGSize, color: UIColor) {
        let circleSize = size.width / 5
        let deltaX = size.width / 2 - circleSize / 2
        let deltaY = size.height / 2 - circleSize / 2
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - size.height) / 2
        let duration: CFTimeInterval = 2
        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "transform")
        
        animation.keyTimes = [0, 0.33, 0.66, 1]
        animation.timingFunctions = [timingFunction, timingFunction, timingFunction]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Top-center circle
        let topCenterCircle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .ring)
        
        changeAnimation(animation, values: ["{0,0}", "{hx,fy}", "{-hx,fy}", "{0,0}"], deltaX: deltaX, deltaY: deltaY)
        topCenterCircle.frame = CGRect(x: x + size.width / 2 - circleSize / 2, y: y, width: circleSize, height: circleSize)
        topCenterCircle.add(animation, forKey: "animation")
        layer.addSublayer(topCenterCircle)
        
        // Bottom-left circle
        let bottomLeftCircle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .ring)
        
        changeAnimation(animation, values: ["{0,0}", "{hx,-fy}", "{fx,0}", "{0,0}"], deltaX: deltaX, deltaY: deltaY)
        bottomLeftCircle.frame = CGRect(x: x, y: y + size.height - circleSize, width: circleSize, height: circleSize)
        bottomLeftCircle.add(animation, forKey: "animation")
        layer.addSublayer(bottomLeftCircle)
        
        // Bottom-right circle
        let bottomRightCircle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .ring)
        
        changeAnimation(animation, values: ["{0,0}", "{-fx,0}", "{-hx,-fy}", "{0,0}"], deltaX: deltaX, deltaY: deltaY)
        bottomRightCircle.frame = CGRect(x: x + size.width - circleSize, y: y + size.height - circleSize, width: circleSize, height: circleSize)
        bottomRightCircle.add(animation, forKey: "animation")
        layer.addSublayer(bottomRightCircle)
    }
    
    // -----------------------------------------------------------------------
    
    func changeAnimation(_ animation: CAKeyframeAnimation, values rawValues: [String], deltaX: CGFloat, deltaY: CGFloat) {
        let values = NSMutableArray(capacity: 5)
        
        for rawValue in rawValues {
            let point = CGPointFromString(translateString(rawValue, deltaX: deltaX, deltaY: deltaY))
            
            values.add(NSValue(caTransform3D: CATransform3DMakeTranslation(point.x, point.y, 0)))
        }
        animation.values = values as [AnyObject]
    }
    
    // -----------------------------------------------------------------------
    
    func translateString(_ valueString: String, deltaX: CGFloat, deltaY: CGFloat) -> String {
        let valueMutableString = NSMutableString(string: valueString)
        let fullDeltaX = 2 * deltaX
        let fullDeltaY = 2 * deltaY
        var range = NSMakeRange(0, valueMutableString.length)
        
        valueMutableString.replaceOccurrences(of: "hx", with: "\(deltaX)", options: NSString.CompareOptions.caseInsensitive, range: range)
        range.length = valueMutableString.length
        valueMutableString.replaceOccurrences(of: "fx", with: "\(fullDeltaX)", options: NSString.CompareOptions.caseInsensitive, range: range)
        range.length = valueMutableString.length
        valueMutableString.replaceOccurrences(of: "hy", with: "\(deltaY)", options: NSString.CompareOptions.caseInsensitive, range: range)
        range.length = valueMutableString.length
        valueMutableString.replaceOccurrences(of: "fy", with: "\(fullDeltaY)", options: NSString.CompareOptions.caseInsensitive, range: range)
        
        return valueMutableString as String
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Audio Equalizer Loading Indicator animation methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationAudioEqaulizer(in layer: CALayer, size: CGSize, color: UIColor) {
        let lineSize = size.width / 9
        let x = (layer.bounds.size.width - lineSize * 7) / 2
        let y = (layer.bounds.size.height - size.height) / 2
        let duration: [CFTimeInterval] = [4.3, 2.5, 1.7, 3.1]
        let values = [0, 0.7, 0.4, 0.05, 0.95, 0.3, 0.9, 0.4, 0.15, 0.18, 0.75, 0.01]
        
        // Draw lines
        for i in 0 ..< 4 {
            let animation = CAKeyframeAnimation()
            
            animation.keyPath = "path"
            animation.isAdditive = true
            animation.values = []
            
            for j in 0 ..< values.count {
                let heightFactor = values[j]
                let height = size.height * CGFloat(heightFactor)
                let point = CGPoint(x: 0, y: size.height - height)
                let path = UIBezierPath(rect: CGRect(origin: point, size: CGSize(width: lineSize, height: height)))
                
                animation.values?.append(path.cgPath)
            }
            animation.duration = duration[i]
            animation.repeatCount = HUGE
            animation.isRemovedOnCompletion = false
            
            let line = layerWith(CGSize(width: lineSize, height: size.height), color: color, animType: .line)
            
            let frame = CGRect(x: x + lineSize * 2 * CGFloat(i),
                               y: y,
                               width: lineSize,
                               height: size.height)
            
            line.frame = frame
            line.add(animation, forKey: "animation")
            layer.addSublayer(line)
        }
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - SemiCircleSpin loading indicator animation method
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationSemiCircleSpin(in layer: CALayer, size: CGSize, color: UIColor) {
        let duration: CFTimeInterval = 1.50
        
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        
        animation.keyTimes = [0, 0.5, 1]
        animation.values = [0, CGFloat.pi, 2 * CGFloat.pi]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circle
        let circle = layerWith(size, color: color, animType: .circleSemi)
        
        let frame = CGRect(
            x: (layer.bounds.width - size.width) / 2,
            y: (layer.bounds.height - size.height) / 2,
            width: size.width,
            height: size.height
        )
        
        circle.frame = frame
        circle.add(animation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - SpinBallPulse loading indicator methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationSpinBallPulse(in layer: CALayer, size: CGSize, color: UIColor) {
        let duration: CFTimeInterval = 1
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.09, 0.57, 0.49, 0.9)
        
        smallCircleWith(duration, timingFunction: timingFunction, layer: layer, size: size, color: color)
        bigCircleWith(duration, timingFunction: timingFunction, layer: layer, size: size, color: color)
    }
    
    // -----------------------------------------------------------------------
    
    func smallCircleWith(_ duration: CFTimeInterval, timingFunction: CAMediaTimingFunction, layer: CALayer, size: CGSize, color: UIColor) {
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        animation.keyTimes = [0, 0.3, 1]
        animation.timingFunctions = [timingFunction, timingFunction]
        animation.values = [1, 0.3, 1]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circle
        let circleSize = size.width / 2
        let circle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
        
        let frame = CGRect(x: (layer.bounds.size.width - circleSize) / 2,
                           y: (layer.bounds.size.height - circleSize) / 2,
                           width: circleSize,
                           height: circleSize)
        
        circle.frame = frame
        circle.add(animation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    
    func bigCircleWith(_ duration: CFTimeInterval, timingFunction: CAMediaTimingFunction, layer: CALayer, size: CGSize, color: UIColor) {
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.5, 1]
        scaleAnimation.timingFunctions = [timingFunction, timingFunction]
        scaleAnimation.values = [1, 0.6, 1]
        scaleAnimation.duration = duration
        
        // Rotate animation
        let rotateAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        
        rotateAnimation.keyTimes = scaleAnimation.keyTimes
        rotateAnimation.timingFunctions = [timingFunction, timingFunction]
        rotateAnimation.values = [0, CGFloat.pi, 2 * CGFloat.pi]
        rotateAnimation.duration = duration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, rotateAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circle
        let circle = layerWith(size, color: color, animType: .ringTwoHalfVertical)
        
        let frame = CGRect(x: (layer.bounds.size.width - size.width) / 2,
                           y: (layer.bounds.size.height - size.height) / 2,
                           width: size.width,
                           height: size.height)
        circle.frame = frame
        circle.add(animation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - BallRotateChase Loading Indicator animation methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationBallRotateChase(in layer: CALayer, size: CGSize, color: UIColor) {
        let circleSize = size.width / 5
        
        // Draw circles
        for i in 0 ..< 5 {
            let factor = Float(i) * 1.0 / 5
            let circle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
            
            let animation = rotateAnimation(factor, x: layer.bounds.size.width / 2, y: layer.bounds.size.height / 2, size: CGSize(width: size.width - circleSize, height: size.height - circleSize))
            
            circle.frame = CGRect(x: 0, y: 0, width: circleSize, height: circleSize)
            circle.add(animation, forKey: "animation")
            layer.addSublayer(circle)
        }
    }
    
    // -----------------------------------------------------------------------
    
    func rotateAnimation(_ rate: Float, x: CGFloat, y: CGFloat, size: CGSize) -> CAAnimationGroup {
        let duration: CFTimeInterval = 1.5
        let fromScale = 1 - rate
        let toScale = 0.2 + rate
        let timeFunc = CAMediaTimingFunction(controlPoints: 0.5, 0.15 + rate, 0.25, 1.0)
        
        // Scale animation
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = duration
        scaleAnimation.repeatCount = HUGE
        scaleAnimation.fromValue = fromScale
        scaleAnimation.toValue = toScale
        
        // Position animation
        let positionAnimation = CAKeyframeAnimation(keyPath: "position")
        positionAnimation.duration = duration
        positionAnimation.repeatCount = HUGE
        positionAnimation.path = UIBezierPath(arcCenter: CGPoint(x: x, y: y), radius: size.width / 2, startAngle: 3 * CGFloat(Double.pi) * 0.5, endAngle: 3 * CGFloat(Double.pi) * 0.5 + 2 * CGFloat(Double.pi), clockwise: true).cgPath
        
        // Aniamtion
        let animation = CAAnimationGroup()
        animation.animations = [scaleAnimation, positionAnimation]
        animation.timingFunction = timeFunc
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        return animation
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - RoundPulseSync loading indicator methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationRoundPulseSync(in layer: CALayer, size: CGSize, color: UIColor) {
        let circleSpacing: CGFloat = 2
        let circleSize = (size.width - circleSpacing * 2) / 3
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - circleSize) / 2
        let deltaY = (size.height / 2 - circleSize / 2) / 2
        let duration: CFTimeInterval = 0.6
        let beginTime = CACurrentMediaTime()
        let beginTimes: [CFTimeInterval] = [0.07, 0.14, 0.21]
        let timingFunciton = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        
        animation.keyTimes = [0, 0.33, 0.66, 1]
        animation.timingFunctions = [timingFunciton, timingFunciton, timingFunciton]
        animation.values = [0, deltaY, -deltaY, 0]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circles
        for i in 0 ..< 3 {
            let circle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
            
            let frame = CGRect(x: x + circleSize * CGFloat(i) + circleSpacing * CGFloat(i),
                               y: y,
                               width: circleSize,
                               height: circleSize)
            
            animation.beginTime = beginTime + beginTimes[i]
            circle.frame = frame
            circle.add(animation, forKey: "animation")
            layer.addSublayer(circle)
        }
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - BallGridPulse Loading indicator animation methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationBallGridPulse(in layer: CALayer, size: CGSize, color: UIColor) {
        let circleSpacing: CGFloat = 2
        let circleSize = (size.width - circleSpacing * 2) / 3
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - size.height) / 2
        let durations: [CFTimeInterval] = [0.72, 1.02, 1.28, 1.42, 1.45, 1.18, 0.87, 1.45, 1.06]
        let beginTime = CACurrentMediaTime()
        let beginTimes: [CFTimeInterval] = [-0.06, 0.25, -0.17, 0.48, 0.31, 0.03, 0.46, 0.78, 0.45]
        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
        
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.5, 1]
        scaleAnimation.timingFunctions = [timingFunction, timingFunction]
        scaleAnimation.values = [1, 0.5, 1]
        
        // Opacity animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        
        opacityAnimation.keyTimes = [0, 0.5, 1]
        opacityAnimation.timingFunctions = [timingFunction, timingFunction]
        opacityAnimation.values = [1, 0.7, 1]
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circles
        for i in 0 ..< 3 {
            for j in 0 ..< 3 {
                let circle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
                
                let frame = CGRect(x: x + circleSize * CGFloat(j) + circleSpacing * CGFloat(j),
                                   y: y + circleSize * CGFloat(i) + circleSpacing * CGFloat(i),
                                   width: circleSize,
                                   height: circleSize)
                
                animation.duration = durations[3 * i + j]
                animation.beginTime = beginTime + beginTimes[3 * i + j]
                circle.frame = frame
                circle.add(animation, forKey: "animation")
                layer.addSublayer(circle)
            }
        }
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - BallPulseRise loading Indicator animation methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationBallPulseRise(in layer: CALayer, size: CGSize, color: UIColor) {
        let circleSpacing: CGFloat = 2
        let circleSize = (size.width - 4 * circleSpacing) / 5
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - circleSize) / 2
        let deltaY = size.height / 2
        let duration: CFTimeInterval = 1
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.15, 0.46, 0.9, 0.6)
        let oddAnimation = self.oddAnimation(duration, deltaY: deltaY, timingFunction: timingFunction)
        let evenAnimation = self.evenAnimation(duration, deltaY: deltaY, timingFunction: timingFunction)
        
        // Draw circles
        for i in 0 ..< 5 {
            let circle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
            
            let frame = CGRect(x: x + circleSize * CGFloat(i) + circleSpacing * CGFloat(i),
                               y: y,
                               width: circleSize,
                               height: circleSize)
            
            circle.frame = frame
            if i % 2 == 0 {
                circle.add(evenAnimation, forKey: "animation")
            } else {
                circle.add(oddAnimation, forKey: "animation")
            }
            layer.addSublayer(circle)
        }
    }
    
    // -----------------------------------------------------------------------
    
    func oddAnimation(_ duration: CFTimeInterval, deltaY: CGFloat, timingFunction: CAMediaTimingFunction) -> CAAnimation {
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.5, 1]
        scaleAnimation.timingFunctions = [timingFunction, timingFunction]
        scaleAnimation.values = [0.4, 1.1, 0.75]
        scaleAnimation.duration = duration
        
        // Translate animation
        let translateAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        
        translateAnimation.keyTimes = [0, 0.25, 0.75, 1]
        translateAnimation.timingFunctions = [timingFunction, timingFunction, timingFunction]
        translateAnimation.values = [0, deltaY, -deltaY, 0]
        translateAnimation.duration = duration
        
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, translateAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        return animation
    }
    
    // -----------------------------------------------------------------------
    
    func evenAnimation(_ duration: CFTimeInterval, deltaY: CGFloat, timingFunction: CAMediaTimingFunction) -> CAAnimation {
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.5, 1]
        scaleAnimation.timingFunctions = [timingFunction, timingFunction]
        scaleAnimation.values = [1.1, 0.4, 1]
        scaleAnimation.duration = duration
        
        // Translate animation
        let translateAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        
        translateAnimation.keyTimes = [0, 0.25, 0.75, 1]
        translateAnimation.timingFunctions = [timingFunction, timingFunction, timingFunction]
        translateAnimation.values = [0, -deltaY, deltaY, 0]
        translateAnimation.duration = duration
        
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, translateAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        return animation
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - BallSpinRotate loading indicator animation methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationBallSpinRotate(in layer: CALayer, size: CGSize, color: UIColor) {
        let circleSize: CGFloat = size.width / 5
        let duration: CFTimeInterval = 1
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.7, -0.13, 0.22, 0.86)
        
        // Scale animationBallSpinRotate
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.5, 1]
        scaleAnimation.timingFunctions = [timingFunction, timingFunction]
        scaleAnimation.values = [1, 0.6, 1]
        scaleAnimation.duration = duration
        
        // Rotate animation
        let rotateAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        
        rotateAnimation.keyTimes = [0, 0.5, 1]
        rotateAnimation.timingFunctions = [timingFunction, timingFunction]
        rotateAnimation.values = [0, Double.pi, 2 * Double.pi]
        rotateAnimation.duration = duration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, rotateAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circles
        let leftCircle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
        let rightCircle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
        let centerCircle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
        
        leftCircle.opacity = 0.8
        leftCircle.frame = CGRect(x: 0, y: (size.height - circleSize) / 2, width: circleSize, height: circleSize)
        rightCircle.opacity = 0.8
        rightCircle.frame = CGRect(x: size.width - circleSize, y: (size.height - circleSize) / 2, width: circleSize, height: circleSize)
        centerCircle.frame = CGRect(x: (size.width - circleSize) / 2, y: (size.height - circleSize) / 2, width: circleSize, height: circleSize)
        
        let circle = CALayer()
        let frame = CGRect(x: (layer.bounds.size.width - size.width) / 2, y: (layer.bounds.size.height - size.height) / 2, width: size.width, height: size.height)
        
        circle.frame = frame
        circle.addSublayer(leftCircle)
        circle.addSublayer(rightCircle)
        circle.addSublayer(centerCircle)
        circle.add(animation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - BallScaleMultiple loading indicator animation methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationBallScaleMultiple(in layer: CALayer, size: CGSize, color: UIColor) {
        
        let duration: CFTimeInterval = 1
        let beginTime = CACurrentMediaTime()
        let beginTimes = [0, 0.2, 0.4]
        
        // Scale animation
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        
        scaleAnimation.duration = duration
        scaleAnimation.fromValue = 0
        scaleAnimation.toValue = 1
        
        // Opacity animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        
        opacityAnimation.duration = duration
        opacityAnimation.keyTimes = [0, 0.05, 1]
        opacityAnimation.values = [0, 1, 0]
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw balls
        for i in 0 ..< 3 {
            let circle = layerWith(size, color: color, animType: .circle)
            let frame = CGRect(x: (layer.bounds.size.width - size.width) / 2,
                               y: (layer.bounds.size.height - size.height) / 2,
                               width: size.width,
                               height: size.height)
            
            animation.beginTime = beginTime + beginTimes[i]
            circle.frame = frame
            circle.opacity = 0
            circle.add(animation, forKey: "animation")
            layer.addSublayer(circle)
        }
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - CustomRound loading indicator animation methods
    
    // -----------------------------------------------------------------------
    func setUpAnimationCustomRound(in layer: CALayer, size: CGSize, color: UIColor) {
        circleInLayer(layer, size: size, color: color)
        pacmanInLayer(layer, size: size, color: color)
    }
    
    // -----------------------------------------------------------------------
    
    func pacmanInLayer(_ layer: CALayer, size: CGSize, color: UIColor) {
        let pacmanSize = 2 * size.width / 3
        let pacmanDuration: CFTimeInterval = 0.5
        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
        
        // Stroke start animation
        let strokeStartAnimation = CAKeyframeAnimation(keyPath: "strokeStart")
        
        strokeStartAnimation.keyTimes = [0, 0.5, 1]
        strokeStartAnimation.timingFunctions = [timingFunction, timingFunction]
        strokeStartAnimation.values = [0.125, 0, 0.125]
        strokeStartAnimation.duration = pacmanDuration
        
        // Stroke end animation
        let strokeEndAnimation = CAKeyframeAnimation(keyPath: "strokeEnd")
        
        strokeEndAnimation.keyTimes = [0, 0.5, 1]
        strokeEndAnimation.timingFunctions = [timingFunction, timingFunction]
        strokeEndAnimation.values = [0.875, 1, 0.875]
        strokeEndAnimation.duration = pacmanDuration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [strokeStartAnimation, strokeEndAnimation]
        animation.duration = pacmanDuration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw pacman
        let pacman = layerWith(CGSize(width: pacmanSize, height: pacmanSize), color: color, animType: .pacman)
        
        let frame = CGRect(
            x: (layer.bounds.size.width - size.width) / 2,
            y: (layer.bounds.size.height - pacmanSize) / 2,
            width: pacmanSize,
            height: pacmanSize
        )
        
        pacman.frame = frame
        pacman.add(animation, forKey: "animation")
        layer.addSublayer(pacman)
    }
    
    // -----------------------------------------------------------------------
    
    func circleInLayer(_ layer: CALayer, size: CGSize, color: UIColor) {
        let circleSize = size.width / 5
        let circleDuration: CFTimeInterval = 1
        
        // Translate animation
        let translateAnimation = CABasicAnimation(keyPath: "transform.translation.x")
        
        translateAnimation.fromValue = 0
        translateAnimation.toValue = -size.width / 2
        translateAnimation.duration = circleDuration
        
        // Opacity animation
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        
        opacityAnimation.fromValue = 1
        opacityAnimation.toValue = 0.7
        opacityAnimation.duration = circleDuration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [translateAnimation, opacityAnimation]
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = circleDuration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circles
        let circle = layerWith(CGSize(width: circleSize, height: circleSize), color: color, animType: .circle)
        
        let frame = CGRect(
            x: (layer.bounds.size.width - size.width) / 2 + size.width - circleSize,
            y: (layer.bounds.size.height - circleSize) / 2,
            width: circleSize,
            height: circleSize
        )
        
        circle.frame = frame
        circle.add(animation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - CustomOrbit loading indicator animation methods
    
    // -----------------------------------------------------------------------
    let duration: CFTimeInterval = 1.9
    let satelliteCoreRatio: CGFloat = 0.25
    let distanceRatio: CGFloat = 1.5 // distance / core size
    
    var coreSize: CGFloat = 0
    var satelliteSize: CGFloat = 0
    
    func setUpAnimationCustomOrbit(in layer: CALayer, size: CGSize, color: UIColor) {
        coreSize = size.width / (1 + satelliteCoreRatio + distanceRatio)
        satelliteSize = coreSize * satelliteCoreRatio
        
        ring1InLayer(layer, size: size, color: color)
        ring2InLayer(layer, size: size, color: color)
        coreInLayer(layer, size: size, color: color)
        satelliteInLayer(layer, size: size, color: color)
    }
    
    // -----------------------------------------------------------------------
    
    func ring1InLayer(_ layer: CALayer, size: CGSize, color: UIColor) {
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.45, 0.45, 1]
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        scaleAnimation.values = [0, 0, 1.3, 2]
        scaleAnimation.duration = duration
        
        // Opacity animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.19, 1, 0.22, 1)
        
        opacityAnimation.keyTimes = [0, 0.45, 1]
        scaleAnimation.timingFunctions = [CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear), timingFunction]
        opacityAnimation.values = [0.8, 0.8, 0]
        opacityAnimation.duration = duration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circle
        let circle = layerWith(CGSize(width: coreSize, height: coreSize), color: color, animType: .circle)
        let frame = CGRect(x: (layer.bounds.size.width - coreSize) / 2,
                           y: (layer.bounds.size.height - coreSize) / 2,
                           width: coreSize,
                           height: coreSize)
        
        circle.frame = frame
        circle.add(animation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    
    func ring2InLayer(_ layer: CALayer, size: CGSize, color: UIColor) {
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.55, 0.55, 1]
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        scaleAnimation.values = [0, 0, 1.3, 2.1]
        scaleAnimation.duration = duration
        
        // Opacity animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.19, 1, 0.22, 1)
        
        opacityAnimation.keyTimes = [0, 0.55, 0.65, 1]
        scaleAnimation.timingFunctions = [CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear), timingFunction]
        opacityAnimation.values = [0.7, 0.7, 0, 0]
        opacityAnimation.duration = duration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circle
        let circle = layerWith(CGSize(width: coreSize, height: coreSize), color: color, animType: .circle)
        let frame = CGRect(x: (layer.bounds.size.width - coreSize) / 2,
                           y: (layer.bounds.size.height - coreSize) / 2,
                           width: coreSize,
                           height: coreSize)
        
        circle.frame = frame
        circle.add(animation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    
    func coreInLayer(_ layer: CALayer, size: CGSize, color: UIColor) {
        let inTimingFunction = CAMediaTimingFunction(controlPoints: 0.7, 0, 1, 0.5)
        let outTimingFunction = CAMediaTimingFunction(controlPoints: 0, 0.7, 0.5, 1)
        let standByTimingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.45, 0.55, 1]
        scaleAnimation.timingFunctions = [inTimingFunction, standByTimingFunction, outTimingFunction]
        scaleAnimation.values = [1, 1.3, 1.3, 1]
        scaleAnimation.duration = duration
        scaleAnimation.repeatCount = HUGE
        scaleAnimation.isRemovedOnCompletion = false
        
        // Draw circle
        let circle = layerWith(CGSize(width: coreSize, height: coreSize), color: color, animType: .circle)
        let frame = CGRect(x: (layer.bounds.size.width - coreSize) / 2,
                           y: (layer.bounds.size.height - coreSize) / 2,
                           width: coreSize,
                           height: coreSize)
        
        circle.frame = frame
        circle.add(scaleAnimation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    
    func satelliteInLayer(_ layer: CALayer, size: CGSize, color: UIColor) {
        // Rotate animation
        let rotateAnimation = CAKeyframeAnimation(keyPath: "position")
        
        rotateAnimation.path = UIBezierPath(arcCenter: CGPoint(x: layer.bounds.midX, y: layer.bounds.midY),
                                            radius: (size.width - satelliteSize) / 2,
                                            startAngle: CGFloat(Double.pi) * 1.5,
                                            endAngle: CGFloat(Double.pi) * 1.5 + 4 * CGFloat(Double.pi),
                                            clockwise: true).cgPath
        rotateAnimation.duration = duration * 2
        rotateAnimation.repeatCount = HUGE
        rotateAnimation.isRemovedOnCompletion = false
        
        // Draw circle
        let circle = layerWith(CGSize(width: satelliteSize, height: satelliteSize), color: color, animType: .circle)
        let frame = CGRect(x: 0, y: 0, width: satelliteSize, height: satelliteSize)
        
        circle.frame = frame
        circle.add(rotateAnimation, forKey: "animation")
        layer.addSublayer(circle)
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - TriangleSkewSpin loading indicator animation methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationTriangleSkewSpin(in layer: CALayer, size: CGSize, color: UIColor) {
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - size.height) / 2
        let duration: CFTimeInterval = 3
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.09, 0.57, 0.49, 0.9)
        
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "transform")
        
        animation.keyTimes = [0, 0.25, 0.5, 0.75, 1]
        animation.timingFunctions = [timingFunction, timingFunction, timingFunction, timingFunction]
        animation.values = [
            NSValue(caTransform3D: CATransform3DConcat(createXTransform(0), createRotateYTransform(0))),
            NSValue(caTransform3D: CATransform3DConcat(createXTransform(CGFloat(Double.pi)), createYTransform(0))),
            NSValue(caTransform3D: CATransform3DConcat(createXTransform(CGFloat(Double.pi)), createYTransform(CGFloat(Double.pi)))),
            NSValue(caTransform3D: CATransform3DConcat(createXTransform(0), createYTransform(CGFloat(Double.pi)))),
            NSValue(caTransform3D: CATransform3DConcat(createXTransform(0), createYTransform(0)))]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw triangle
        let triangle = layerWith(size, color: color, animType: .triangle)
        
        triangle.frame = CGRect(x: x, y: y, width: size.width, height: size.height)
        triangle.add(animation, forKey: "animation")
        layer.addSublayer(triangle)
    }
    
    // -----------------------------------------------------------------------
    
    func createXTransform(_ angle: CGFloat) -> CATransform3D {
        var transform = CATransform3DMakeRotation(angle, 1, 0, 0)
        
        transform.m34 = CGFloat(-1) / 100
        
        return transform
    }
    
    // -----------------------------------------------------------------------
    
    func createYTransform(_ angle: CGFloat) -> CATransform3D {
        var transform = CATransform3DMakeRotation(angle, 0, 1, 0)
        
        transform.m34 = CGFloat(-1) / 100
        
        return transform
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - LineScalePulseOutRapid loading indicator animation methods
    
    // -----------------------------------------------------------------------
    
    func setUpAnimationLineScalePulseOutRapid(in layer: CALayer, size: CGSize, color: UIColor) {
        let lineSize = size.width / 9
        let x = (layer.bounds.size.width - size.width) / 2
        let y = (layer.bounds.size.height - size.height) / 2
        let duration: CFTimeInterval = 0.9
        let beginTime = CACurrentMediaTime()
        let beginTimes = [0.5, 0.25, 0, 0.25, 0.5]
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.11, 0.49, 0.38, 0.78)
        
        // Animation
        let animation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        
        animation.keyTimes = [0, 0.8, 0.9]
        animation.timingFunctions = [timingFunction, timingFunction]
        animation.beginTime = beginTime
        animation.values = [1, 0.3, 1]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw lines
        for i in 0 ..< 5 {
            let line = layerWith(CGSize(width: lineSize, height: size.height), color: color, animType: .line)
            
            let frame = CGRect(x: x + lineSize * 2 * CGFloat(i),
                               y: y,
                               width: lineSize,
                               height: size.height)
            
            animation.beginTime = beginTime + beginTimes[i]
            line.frame = frame
            line.add(animation, forKey: "animation")
            layer.addSublayer(line)
        }
    }
    
    // -----------------------------------------------------------------------
    // MARK: -
    // MARK: - Animation SubType Methods
    
    // -----------------------------------------------------------------------
    
    fileprivate func layerWith(_ size: CGSize, color: UIColor, animType: AnimationSubType) -> CALayer {
        let layer: CAShapeLayer = CAShapeLayer()
        var path: UIBezierPath = UIBezierPath()
        let lineWidth: CGFloat = 2
        
        switch animType {
        case .circle:
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: 0,
                        endAngle: CGFloat(2 * Double.pi),
                        clockwise: false)
            layer.fillColor = color.cgColor
        case .circleSemi:
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: CGFloat(-Double.pi / 6),
                        endAngle: CGFloat(-5 * Double.pi / 6),
                        clockwise: false)
            path.close()
            layer.fillColor = color.cgColor
        case .ring:
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: 0,
                        endAngle: CGFloat(2 * Double.pi),
                        clockwise: false)
            layer.fillColor = nil
            layer.strokeColor = color.cgColor
            layer.lineWidth = lineWidth
        case .ringTwoHalfVertical:
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: CGFloat(-3 * (Double.pi / 4)),
                        endAngle: CGFloat(-(Double.pi / 4)),
                        clockwise: true)
            path.move(
                to: CGPoint(x: size.width / 2 - size.width / 2 * CGFloat(cos(Double.pi / 4)),
                            y: size.height / 2 + size.height / 2 * CGFloat(sin(Double.pi / 4)))
            )
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: CGFloat(-5 * Double.pi / 4),
                        endAngle: CGFloat(-7 * Double.pi / 4),
                        clockwise: false)
            layer.fillColor = nil
            layer.strokeColor = color.cgColor
            layer.lineWidth = lineWidth
        case .ringTwoHalfHorizontal:
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: CGFloat(3 * Double.pi / 4),
                        endAngle: CGFloat(5 * Double.pi / 4),
                        clockwise: true)
            path.move(
                to: CGPoint(x: size.width / 2 + size.width / 2 * CGFloat(cos(Double.pi / 4)),
                            y: size.height / 2 - size.height / 2 * CGFloat(sin(Double.pi / 4)))
            )
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: CGFloat(-Double.pi / 4),
                        endAngle: CGFloat(Double.pi / 4),
                        clockwise: true)
            layer.fillColor = nil
            layer.strokeColor = color.cgColor
            layer.lineWidth = lineWidth
        case .ringThirdFour:
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: CGFloat(-3 * Double.pi / 4),
                        endAngle: CGFloat(-Double.pi / 4),
                        clockwise: false)
            layer.fillColor = nil
            layer.strokeColor = color.cgColor
            layer.lineWidth = 2.5
        case .rectangle:
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: size.width, y: 0))
            path.addLine(to: CGPoint(x: size.width, y: size.height))
            path.addLine(to: CGPoint(x: 0, y: size.height))
            layer.fillColor = color.cgColor
        case .triangle:
            let offsetY = size.height / 4
            
            path.move(to: CGPoint(x: 0, y: size.height - offsetY))
            path.addLine(to: CGPoint(x: size.width / 2, y: size.height / 2 - offsetY))
            path.addLine(to: CGPoint(x: size.width, y: size.height - offsetY))
            path.close()
            layer.fillColor = color.cgColor
        case .line:
            path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: size.width, height: size.height),
                                cornerRadius: size.width / 2)
            layer.fillColor = color.cgColor
        case .pacman:
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 4,
                        startAngle: 0,
                        endAngle: CGFloat(2 * Double.pi),
                        clockwise: true)
            layer.fillColor = nil
            layer.strokeColor = color.cgColor
            layer.lineWidth = size.width / 2
        }
        
        layer.backgroundColor = nil
        layer.path = path.cgPath
        layer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        return layer
    }
}
