//
//  ValidatableField.swift
//  ValidationManager


import Foundation
import UIKit

public typealias ValidatableField = Any & Validatable

public protocol Validatable {

    var validationText: String {
        get
    }
}

extension UITextField: Validatable {

    public var validationText: String {
        return text ?? ""
    }
}

//extension UITextView: Validatable {
//    public var validationText: String {
//        return text ?? ""
//    }
//}
//
//extension UIImageView: Validatable {
//    public var validationText: String {
//        return (self.image != nil) ? "Found" : ""
//    }
//}
