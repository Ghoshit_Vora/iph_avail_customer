//
//  NoInternetConnectionVC.swift
//  IPH_Avail_Customer
//
//  Created on 03/06/18.


import UIKit

protocol NoInternetDelegate: class {
    func tryAgainClicked()
}

class NoInternetConnectionVC: UIViewController {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var viewNoInternet                       : UIView!
    @IBOutlet weak var lblNoInternet                        : UILabel!
    @IBOutlet weak var ivNoInternet                         : UIImageView!
    @IBOutlet weak var lblMessgae                           : UILabel!
    @IBOutlet weak var btnTryAgain                          : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    weak var delegate                                       : NoInternetDelegate?
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnTryAgainTapped(_ sender: UIButton) {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() {
            self.delegate?.tryAgainClicked()
            self.view.removeFromSuperview()
        }
        
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
