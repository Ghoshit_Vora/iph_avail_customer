//
//  LegalVC.swift
//  IPH_Avail_Customer
//
//  Created on 03/07/18.


import UIKit
import ObjectMapper

class LegalVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var tblLegal                     : UITableView!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var arrLegalList                                : [AVPageList]?
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Abstract Methods
    //---------------------------------------------------------------------------
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    
    
    
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setupView() {
        self.title = "Legal"
        
        self.setupLeftButtonWithTitle(isMenuRequired: true)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = UIColor.black
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Web Service Call Methods
    //---------------------------------------------------------------------------
    
    func getLegalData() {
        
        DataManager.sharedManager.getLegalData(success: { (responseData, responseError) in
            
            if responseError == nil,
                let responseData = Mapper<AVLegalResponse>().map(JSONObject: responseData),
                let arrLegal = responseData.pageList {
                
                self.arrLegalList = arrLegal
                
                self.tblLegal.reloadData()
                
            }
            
        }) { (error) in
            self.showAlert(App.AppName, message: error.localizedDescription)
        }
    }
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.getLegalData()
    }
}



//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UITableViewDataSource Methods
//---------------------------------------------------------------------------

extension LegalVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrLegalList != nil {
            return (self.arrLegalList?.count)!
        }
        return 0
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LegalCell", for: indexPath)
        cell.textLabel?.text = arrLegalList![indexPath.row].title
        return cell
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UITableViewDelegate Methods
//---------------------------------------------------------------------------

extension LegalVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if let strUrl = self.arrLegalList![indexPath.row].url,
//            let url = URL.init(string: strUrl),
//            UIApplication.shared.canOpenURL(url) {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        }
        
        let vc = AppStoryboard.AboutUs.viewController(viewControllerClass: LegalDetailsVC.self)
        vc.lblText = self.arrLegalList![indexPath.row].title!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}












