//
//  RegisterVC.swift
//  IPH_Avail_Customer
//
// Created on 10/05/18.


import UIKit
import ObjectMapper

class RegisterVC: BaseVC {

    //MARK:-Properties
    
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtContryCode: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtEmailId: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var strOTP = String()
    var noInternetVC                                : NoInternetConnectionVC?
    
    //MARK:-Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-Custom Method
    
    func callRegisterWS() {
        
        DataManager.sharedManager.setRegisterData(name: self.txtFullName.text!, email: self.txtEmailId.text!, password: self.txtPassword.text!, countryCode: self.txtContryCode.text!, mobile: self.txtMobileNo.text!, success: { (response, error) in
            
            if error == nil, let responseData = Mapper<AVRegisterRequest>().map(JSONObject: response), let msg = responseData.message {
                
                self.strOTP = msg;
                UserDefaults.Account.set(true, forKey: UserDefaults.Account.BoolDefaultKey.isUserLoggedIn)
                
                if let customerDetails = responseData.customerDetail,
                    let firstObject = customerDetails.first {
                    
                    if let name = firstObject.name {
                        UserDefaults.UserData.set(name as AnyObject, forKey: .name)
                    }
                    
                    if let email = firstObject.email {
                        UserDefaults.UserData.set(email as AnyObject, forKey: .email)
                    }
                    
                    if let phone = firstObject.phone {
                        UserDefaults.UserData.set(phone as AnyObject, forKey: .phone)
                    }
                    
                    if let strImageUrl = firstObject.image {
                        UserDefaults.UserData.set(strImageUrl as AnyObject, forKey: .image)
                    }
                }
                
                let userData = responseData.customerDetail![0]
                UserDefaults.UserData.set(userData.iD as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.userId)
                
                let vc = AppStoryboard.Authentication.viewController(viewControllerClass: OTPVC.self)
                vc.strOTP = self.strOTP
                let navigationController = UINavigationController(rootViewController: vc)
                
                AppDelegate.sharedInstance.window?.rootViewController = navigationController
                
            }
        }) { (error) in
            if error.localizedDescription == ErrorManager.noInternetConnectionError.errorDescription {
                self.noInternetVC = AppStoryboard.Authentication.viewController(viewControllerClass: NoInternetConnectionVC.self)
                AppDelegate.sharedInstance.window?.addSubview((self.noInternetVC?.view)!)
            } else {
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
        }
    }
    
    //MARK:-Action Method
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        
        if sender.title(for: .normal) == "Show" {
            self.btnShow.setTitle("Hide", for: .normal)
            self.txtPassword.isSecureTextEntry = false
        } else {
            self.btnShow.setTitle("Show", for: .normal)
            self.txtPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnRegisterTapped(_ sender: Any) {
        
        let fieldValidator = Validator()

        fieldValidator.registerField(self.txtFullName, rule: [RequiredRule(message: "Please enter full name")])


        fieldValidator.registerField(self.txtMobileNo, rule: [RequiredRule(message: "Please enter phone no"), PhoneNumberRule(message: "Please enter valid phone no")])


        fieldValidator.registerField(self.txtEmailId, rule: [RequiredRule(message: "Please enter valid email"), EmailRule(message: "Please enter valid email")])

        fieldValidator.registerField(self.txtPassword, rule: [RequiredRule(message: "Please enter valid password")])

        fieldValidator.validate({
            self.callRegisterWS()
        }) { validationError in
            // Failer
            if validationError.count > 0 {
                self.showAlertWithOKBtn(message: validationError[0].errorMessage)
            }
        }
        
        

    }
    
    @IBAction func btnTermsTapped(_ sender: Any) {
        
        let vc = AppStoryboard.Authentication.viewController(viewControllerClass: TermsVC.self)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //---------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
}
