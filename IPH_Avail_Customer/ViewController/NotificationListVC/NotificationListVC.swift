//
//  NotificationListVC.swift
//  IPH_Avail_Customer
//
// Created on 11/07/18.


import UIKit
import ObjectMapper

class NotificationListVC: BaseVC {

    //MARK:-Properties
    
    @IBOutlet weak var tblNotifcationList: UITableView!
    var arrNotification:[AVOfferList] = []
    var noInternetVC : NoInternetConnectionVC?

    //MARK:-Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-Custom Method
    
    func setUpView() {
        self.callNotifcationWS()
    }
    
    //MARK:-WS Method
    
    func callNotifcationWS() {
        
        DataManager.sharedManager.getNotifcationData(success: { (response, error) in
            
            if error == nil, let responseData = Mapper<AVNotificationRequest>().map(JSONObject: response), let _ = responseData.message {
                
                self.arrNotification = responseData.offerList!
                
                if self.arrNotification.count > 0 {
                    self.tblNotifcationList.isHidden = false
                    self.tblNotifcationList.reloadData()
                }
                
            }
        }, failure: { (error) in
            
            if error.localizedDescription == ErrorManager.noInternetConnectionError.errorDescription {
                self.noInternetVC = AppStoryboard.Authentication.viewController(viewControllerClass: NoInternetConnectionVC.self)
                AppDelegate.sharedInstance.window?.addSubview((self.noInternetVC?.view)!)
            } else {
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
        })
    }
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }

}

extension NotificationListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListCell") as! NotificationListCell
        
        let data = self.arrNotification[indexPath.row]
        
        cell.lblTitle.text = data.title
        cell.lblDescription.text = data.message
        
        let strUrl = URL(string: data.image!)
        cell.imgOffer.sd_setImage(with: strUrl, placeholderImage: UIImage.init())
        return cell
    }
}

extension NotificationListVC : NoInternetDelegate {
    func tryAgainClicked() {
        
        self.callNotifcationWS()
    }
}
