//
//  OTPVC.swift
//  IPH_Avail_Customer
//
// Created on 28/05/18.


import UIKit

class OTPVC: BaseVC {

    //MARK:-Properties
    
    @IBOutlet weak var txtOtp: UITextField!
    var strOTP:String = ""
    
    //MARK:-Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom Methods
    
    func setUpView() {
        
       self.navigationController?.navigationBar.isHidden = true
        
        self.showAlertWithOKBtn(message: strOTP)
    }
    
    //MARK:- WS Method
    
    func sendOTPWS()  {
        
        if let strCustomerId = UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            DataManager.sharedManager.setOTPData(otp: self.txtOtp.text!, custId: strCustomerId, success: { (response, error) in
                
                if error == nil {
                    let vc = AppStoryboard.Dashboard.viewController(viewControllerClass: DashboardVC.self)
                    AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: vc)
                } else {
                    self.showAlertWithOKBtn(message: error.debugDescription)
                }
            }) { (error) in
                
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
        }
        
    }
    
    func resendOTPWS()  {
        
        if let strCustomerId = UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            DataManager.sharedManager.setResendOTPData(custId: strCustomerId, success: { (response, error) in
                
                if error == nil {

                } else {
                    self.showAlertWithOKBtn(message: error.debugDescription)
                }
            }) { (error) in
                
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
        }
    }
    
    //MARK:-Action Method
    
    
    @IBAction func btnOTPSendTapped(_ sender: Any) {
        
        let fieldValidator = Validator()

        fieldValidator.registerField(self.txtOtp, rule: [RequiredRule(message: "Please enter valid otp")])

        fieldValidator.validate({
            self.sendOTPWS()
        }) { validationError in
            // Failer
            if validationError.count > 0 {
                self.showAlertWithOKBtn(message: validationError[0].errorMessage)
            }
        }
        
        
    }
    
    
    @IBAction func btnResendOTPTapped(_ sender: Any) {
        
        self.resendOTPWS()
    }
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }

}
