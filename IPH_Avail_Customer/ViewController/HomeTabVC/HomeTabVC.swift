//
//  HomeTabVC.swift
//  IPH_Avail_Customer
//
// Created on 10/05/18.


import UIKit
import BlowinSwiper

class HomeTabVC: BaseVC,BlowinSwipeable {
    
    //MARK:-Properties
    
    var blowinSwiper: BlowinSwiper?
    var viewControllers = [UIViewController]()
    
    //MARK:-Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-Custom Method
    
    func setUpView()  {
        
        let loginViewController = AppStoryboard.Authentication.viewController(viewControllerClass: LoginVC.self)
        
        let LoginNavigation = UINavigationController(rootViewController: loginViewController)
        
        let RegisterViewController = AppStoryboard.Authentication.viewController(viewControllerClass: RegisterVC.self)
        
        let RegisterNavigation = UINavigationController(rootViewController: RegisterViewController)
        
        viewControllers = [LoginNavigation, RegisterNavigation]
        
        swipeMenuView.reloadData()
        
        self.setUpNavigation(isImgNavigation: true, isAppColorRequired: false)
    
    }
    
    //MARK:-Action Method
    
    @IBOutlet weak var swipeMenuView: SwipeMenuView! {
        didSet {
            swipeMenuView.options.tabView.style                         = .segmented
            swipeMenuView.options.tabView.underlineView.backgroundColor = Theme.AppMainPurpleColor
            swipeMenuView.options.tabView.itemView.textColor            = .lightGray
            swipeMenuView.options.tabView.itemView.selectedTextColor    = Theme.AppMainPurpleColor
            
            swipeMenuView.options.tabView.itemView.font = UIFont.CalibriBOLDFont(with: 14.0)
            
            swipeMenuView.delegate = self
            swipeMenuView.dataSource = self
        }
    }
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Enable scrolling by putting finger back during swipe back
        swipeMenuView.contentScrollView?.isScrollEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureSwipeBack(isInsensitive: true)
        enabledRecognizeSimultaneously(scrollView: swipeMenuView.contentScrollView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Stop the scrollView while swiping back
        swipeMenuView.contentScrollView?.isScrollEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        disabledRecognizeSimultaneously()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        swipeMenuView.willChangeOrientation()
    }
}

extension HomeTabVC: SwipeMenuViewDelegate {
    
    func swipeMenuViewDidScroll(_ contentScrollView: UIScrollView) {
        handleScrollRecognizeSimultaneously(scrollView: contentScrollView)
    }
}

extension HomeTabVC: SwipeMenuViewDataSource {
    
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return viewControllers.count
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        
        if index == 0 {
            return "LOGIN"
        } else {
            return "REGISTER"
        }
        
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        return viewControllers[index]
    }
}
