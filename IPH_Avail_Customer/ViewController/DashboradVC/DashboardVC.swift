//
//  DashboardVC.swift
//  IPH_Avail_Customer


import UIKit
import ObjectMapper
import SDWebImage
import CoreLocation
import GoogleMaps
import GooglePlaces

class DashboardVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var btnViewMore                          : UIButton!
    @IBOutlet weak var lblFirstService                      : UILabel!
    @IBOutlet weak var imgFirstService                      : UIImageView!
    @IBOutlet weak var imgSecondService                     : UIImageView!
    @IBOutlet weak var lblSecondService                     : UILabel!
    @IBOutlet weak var imgThirdService                      : UIImageView!
    @IBOutlet weak var lblThirdService                      : UILabel!
    @IBOutlet weak var imgForthService                      : UIImageView!
    @IBOutlet weak var lblForthService                      : UILabel!
    @IBOutlet weak var imgFifthService                      : UIImageView!
    @IBOutlet weak var lblFifthService                      : UILabel!
    @IBOutlet weak var lblSixthService                      : UILabel!
    @IBOutlet weak var imgSixthService                      : UIImageView!
    @IBOutlet weak var viewServices                         : UIView!
    
    @IBOutlet weak var btnArt                               : UIButton!
    @IBOutlet weak var btnDelivery                          : UIButton!
    @IBOutlet weak var btnHomeImproveMent                   : UIButton!
    @IBOutlet weak var btnLesson                            : UIButton!
    @IBOutlet weak var btnMedical                           : UIButton!
    @IBOutlet weak var btnYardWork                          : UIButton!
    
    @IBOutlet weak var placeView                            : UIView!
    @IBOutlet weak var lblLocation                          : UILabel!
    @IBOutlet weak var btnSearch                            : UIButton!
    @IBOutlet weak var btnCurrentLocation                   : UIButton!
    
    @IBOutlet weak var mapView                              : GMSMapView!
    
    @IBOutlet weak var greenView                            : UIView!
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var attrs = [
        NSAttributedStringKey.font : UIFont.CalibriBOLDFont(with: 14.0),
        NSAttributedStringKey.foregroundColor : Theme.AppMainPurpleColor,
        NSAttributedStringKey.underlineStyle : 1] as [NSAttributedStringKey : Any]
    
    var attributedString = NSMutableAttributedString(string:"")
    var arrCategoryHomeData:[AVCategoryHomeList] = []
    
    var locationManager                             : CLLocationManager?
    var locationCoordinate                          : CLLocationCoordinate2D?
    var destinationMarker                           : GMSMarker?
    var selectedPlace                               : GMSPlace?
    var gmsAutoCompleteViewController               : GMSAutocompleteViewController?
    var isSearchPlaced                              = false
    var currentLocation                             : CLLocationCoordinate2D?
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setUpView() {
        
        let buttonTitleStr = NSMutableAttributedString(string:"View All", attributes:attrs)
        attributedString.append(buttonTitleStr)
        btnViewMore.setAttributedTitle(attributedString, for: .normal)
        
        viewServices.isHidden = true
        
        self.placeView.layer.cornerRadius = 5.0
        
        self.greenView.layer.cornerRadius = self.greenView.frame.size.width / 2
        
        if let strMapStyle = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
            do {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: strMapStyle)
            } catch {
                print("Error to set style = \(error.localizedDescription)")
            }
        }
        
        self.btnCurrentLocation.layer.cornerRadius = self.btnCurrentLocation.frame.size.width / 2
    }
    
    //---------------------------------------------------------------------------
    
    func setHomeCategory() {
        
        if self.arrCategoryHomeData.count > 0 {
            
            viewServices.isHidden = false
            
            self.lblFirstService.text = arrCategoryHomeData[0].name
            self.lblSecondService.text = arrCategoryHomeData[1].name
            self.lblThirdService.text = arrCategoryHomeData[2].name
            self.lblForthService.text = arrCategoryHomeData[3].name
            self.lblFifthService.text = arrCategoryHomeData[4].name
            self.lblSixthService.text = arrCategoryHomeData[5].name
            
            let strUrlOne = URL(string: arrCategoryHomeData[0].icon!)
            self.imgFirstService.sd_setImage(with: strUrlOne, placeholderImage: UIImage.init())
            
            let strUrlTwo = URL(string: arrCategoryHomeData[1].icon!)
            self.imgSecondService.sd_setImage(with: strUrlTwo, placeholderImage: UIImage.init())
            
            let strUrlThree = URL(string: arrCategoryHomeData[2].icon!)
            self.imgThirdService.sd_setImage(with: strUrlThree, placeholderImage: UIImage.init())
            
            let strUrlFour = URL(string: arrCategoryHomeData[3].icon!)
            self.imgForthService.sd_setImage(with: strUrlFour, placeholderImage: UIImage.init())
            
            let strUrlFive = URL(string: arrCategoryHomeData[4].icon!)
            self.imgFifthService.sd_setImage(with: strUrlFive, placeholderImage: UIImage.init())
            
            let strUrlSix = URL(string: arrCategoryHomeData[5].icon!)
            self.imgSixthService.sd_setImage(with: strUrlSix, placeholderImage: UIImage.init())
        }
    }
    
    //---------------------------------------------------------------------------
    
    func openCategory(for index: Int) {
       
        if arrCategoryHomeData[index].subcat == "Yes" {
            let vc = AppStoryboard.Category.viewController(viewControllerClass: SubCategoryVC.self)
            vc.categoryId = arrCategoryHomeData[index].catID!
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = AppStoryboard.Category.viewController(viewControllerClass: PersonListVC.self)
            vc.categoryId = arrCategoryHomeData[index].catID!
            vc.categoryName = arrCategoryHomeData[index].name!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //---------------------------------------------------------------------------
    
    func setupCoreLocation() {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager?.requestAlwaysAuthorization()
            locationManager?.startUpdatingLocation()
        } else {
            self.showAlertWithOKBtn(message: "Please enable location service in your device to update your location.")
        }
    }
    
    //---------------------------------------------------------------------------
    
    func addPin(at coordinate: CLLocationCoordinate2D) {
        
        if self.destinationMarker == nil {
            
            destinationMarker = GMSMarker()
            destinationMarker?.position = coordinate
            destinationMarker?.map = self.mapView
            
            let cameraPostion = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 17)
            
            mapView.camera = cameraPostion
        
        } else {
            
            destinationMarker?.position = coordinate
            destinationMarker?.map = self.mapView
            
            let cameraPostion = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 17)
            
            mapView.camera = cameraPostion
        }
    }
    
    //---------------------------------------------------------------------------
    
    func updateLocation(coordinates: CLLocationCoordinate2D, animationDuration: Double) {
        
        if destinationMarker == nil {
            
            destinationMarker = GMSMarker()
            destinationMarker?.position = coordinates
            destinationMarker?.map = self.mapView
            
        } else {
            CATransaction.begin()
            CATransaction.setAnimationDuration(animationDuration)
            destinationMarker?.position = coordinates
            CATransaction.commit()
        }
    }
    
    //---------------------------------------------------------------------------
    
    func getCurrentAddress(at coordinate: CLLocationCoordinate2D) {
        
        let geoCoder = GMSGeocoder()
        
        geoCoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
            
            if error == nil {
                if let address = response?.firstResult() {
                    let line = address.lines
                    let address = line?.joined(separator: ",")
                    self.lblLocation.text = address
                    BaseVC.currentAddress = address
                }
            } else {
                print("Geocoder error:- \(error!.localizedDescription)")
            }
        }
    }
    
    //---------------------------------------------------------------------------
    
    func movePin(at coordinate: CLLocationCoordinate2D) {

        // Movement
        CATransaction.begin()
        CATransaction.setAnimationDuration(5.0)
        
        // Center Map View
        let cameraPostion = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 17.0)
        mapView.animate(to: cameraPostion)
        
        CATransaction.commit()
    }

    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnViewMoreTapped(_ sender: Any) {
        let vc = AppStoryboard.Category.viewController(viewControllerClass: CategoryVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnArtTapped(_ sender: UIButton) {
        self.openCategory(for: 0)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnDeliveryTapped(_ sender: UIButton) {
        self.openCategory(for: 1)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnHomeImprovementTapped(_ sender: UIButton) {
        self.openCategory(for: 2)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnLessonTapped(_ sender: UIButton) {
        self.openCategory(for: 3)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnMedicalTapped(_ sender: UIButton) {
        self.openCategory(for: 4)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnYardWorkTapped(_ sender: UIButton) {
        self.openCategory(for: 5)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnSearchTapped(_ sender: UIButton) {
        gmsAutoCompleteViewController = GMSAutocompleteViewController()
        gmsAutoCompleteViewController?.delegate = self
        self.navigationController?.present(gmsAutoCompleteViewController!, animated: true, completion: nil)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnCurrentLocationTapped(_ sender: UIButton) {
        
        if let currentLocation = BaseVC.currentPlace {
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(2.0)
            
            // Center Map View
            let cameraPostion = GMSCameraPosition.camera(withLatitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 17.0)
            mapView.animate(to: cameraPostion)
            
            CATransaction.commit()
        }
    }
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- WS Call Methods
    //---------------------------------------------------------------------------
    
    func callDashboardWS() {
        
       if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            
            DataManager.sharedManager.getDashboardData(custId: strCustomerId, success: { (response, error) in
                
                if error == nil, let responseData = Mapper<AVDashboardRequest>().map(JSONObject: response), let _ = responseData.message {
                    
                    self.arrCategoryHomeData = responseData.categoryHomeList!
                    
                    self.setHomeCategory()
                    
                }
            }, failure: { (error) in
                self.showAlertWithOKBtn(message: error.localizedDescription)
            })
        }
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        self.setupCoreLocation()
    }
    
    //---------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        callDashboardWS()
        self.setTitle()
        self.setupLeftButtonWithTitle(isMenuRequired: true)
        self.setupRightButton()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = UIColor.black
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension DashboardVC Methods
//---------------------------------------------------------------------------

extension DashboardVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else {
            return
        }
        BaseVC.currentPlace = locValue
        self.addPin(at: locValue)
        self.getCurrentAddress(at: locValue)
        manager.stopUpdatingLocation()
    }
    
    //---------------------------------------------------------------------------
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Error:- \(error.localizedDescription)")
    }
}

//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension GMSMapViewDelegate Methods
//---------------------------------------------------------------------------

extension DashboardVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if self.isSearchPlaced {
            self.updateLocation(coordinates: position.target, animationDuration: 2.0)
            self.isSearchPlaced = false
        } else {
            self.updateLocation(coordinates: position.target, animationDuration: 0.1)
            self.getCurrentAddress(at: position.target)
        }
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension GMSAutocompleteViewControllerDelegate Methods
//---------------------------------------------------------------------------

extension DashboardVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Selected Place:- \(place)")
        self.isSearchPlaced = true
        viewController.dismiss(animated: true) {
            DispatchQueue.main.async(execute: {
//                self.getCurrentAddress(at: place.coordinate)
                self.movePin(at: place.coordinate)
            })
        }
    }
    
    //---------------------------------------------------------------------------
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true) {
            print("Failed autocomplete")
        }
    }
    
    //---------------------------------------------------------------------------
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true) {
            print("Cancel autocomplete")
        }
    }
}


