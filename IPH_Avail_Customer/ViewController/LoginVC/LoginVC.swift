//
//  LoginVC.swift
//  IPH_Avail_Customer
//
// Created on 10/05/18.


import UIKit
import ObjectMapper

class LoginVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var txtMail                      : UITextField!
    @IBOutlet weak var txtPassword                  : UITextField!
    @IBOutlet weak var btnShow                      : UIButton!
    @IBOutlet weak var btnForgotPassword            : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var noInternetVC                                : NoInternetConnectionVC?
    var forgotPasswordVC                            : ForgotPasswordVC?
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- WS Call Methods
    //---------------------------------------------------------------------------
    
    func callLoginWS()  {
        
        DataManager.sharedManager.setLoginData(email: self.txtMail.text!, password: self.txtPassword.text!.trimmingCharacters(in: .whitespaces), success: { (response, error) in
            
            if error == nil, let responseData = Mapper<AVLoginRequest>().map(JSONObject: response), let _ = responseData.message {
                
                UserDefaults.Account.set(true, forKey: UserDefaults.Account.BoolDefaultKey.isUserLoggedIn)
                
                if let customerDetails = responseData.customerDetail,
                    let firstObject = customerDetails.first {
                    
                    if let name = firstObject.name {
                        UserDefaults.UserData.set(name as AnyObject, forKey: .name)
                    }
                    
                    if let email = firstObject.email {
                        UserDefaults.UserData.set(email as AnyObject, forKey: .email)
                    }
                    
                    if let phone = firstObject.phone {
                        UserDefaults.UserData.set(phone as AnyObject, forKey: .phone)
                    }
                    
                    if let strImageUrl = firstObject.image {
                        UserDefaults.UserData.set(strImageUrl as AnyObject, forKey: .image)
                    }
                }
                
                let userData = responseData.customerDetail![0]
                UserDefaults.UserData.set(userData.iD as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.userId)
                
                let vc = AppStoryboard.Dashboard.viewController(viewControllerClass: DashboardVC.self)
                AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: vc)
                
            } else {
                self.showAlertWithOKBtn(message: "Username and password is wrong")
            }
        }) { (error) in
            
            if error.localizedDescription == ErrorManager.noInternetConnectionError.errorDescription {
                self.noInternetVC = AppStoryboard.Authentication.viewController(viewControllerClass: NoInternetConnectionVC.self)
                AppDelegate.sharedInstance.window?.addSubview((self.noInternetVC?.view)!)
            } else {
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
            
        }
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnLoginTapped(_ sender: Any) {
        
        
            
            let fieldValidator = Validator()
            
            fieldValidator.registerField(self.txtMail, rule: [RequiredRule(message: "Please enter valid email"), EmailRule(message: "Please enter valid email")])
            
            fieldValidator.registerField(self.txtPassword, rule: [RequiredRule(message: "Please enter valid password")])
            
            fieldValidator.validate({
                self.callLoginWS()
            }) { validationError in
                // Failer
                if validationError.count > 0 {
                    self.showAlertWithOKBtn(message: validationError[0].errorMessage)
                }
            }
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnShowTapped(_ sender: UIButton) {
        
        if sender.title(for: .normal) == "Show" {
            self.btnShow.setTitle("Hide", for: .normal)
            self.txtPassword.isSecureTextEntry = false
        } else {
            self.btnShow.setTitle("Show", for: .normal)
            self.txtPassword.isSecureTextEntry = true
        }
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        forgotPasswordVC = AppStoryboard.Authentication.viewController(viewControllerClass: ForgotPasswordVC.self)
        AppDelegate.sharedInstance.window?.addSubview((forgotPasswordVC?.view)!)
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //---------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
}
