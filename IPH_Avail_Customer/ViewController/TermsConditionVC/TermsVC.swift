//
//  TermsVC.swift
//  IPH_Avail_Customer
//
// Created on 28/05/18.


import UIKit

class TermsVC: BaseVC {

    //MARK:-Properties
    
    @IBOutlet weak var webView: UIWebView!
    
    //MARK:-Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom Methods
    
    func setUpView() {
        
        self.navigationController?.navigationBar.barTintColor = Theme.AppMainPurpleColor
     
        let myURL = URL(string: "https://www.google.com")
        let myRequest = URLRequest(url: myURL!)
        webView.loadRequest(myRequest)
        
        let titleLabel = UILabel()
        let colour = UIColor.white
        let attributes = [
            NSAttributedStringKey.font as NSString: UIFont.systemFont(ofSize: 20),
            NSAttributedStringKey.foregroundColor as NSString: colour,
            NSAttributedStringKey.kern as NSString: 5.0 as AnyObject
        ]
        
        titleLabel.attributedText = NSAttributedString(string: "AVAIL", attributes: attributes as [NSAttributedStringKey : Any])
        titleLabel.sizeToFit()
        self.navigationItem.titleView = titleLabel
    }
    
    //MARK:-Action Method
    
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
}
