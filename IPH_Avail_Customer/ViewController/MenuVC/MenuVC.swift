//
//  MenuVC.swift


import UIKit
import SJSwiftSideMenuController
import SDWebImage

class MenuVC: UIViewController {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var tblSideMenu                  : UITableView!
    @IBOutlet weak var lblName                      : UILabel!
    @IBOutlet weak var lblPhoneNo                   : UILabel!
    @IBOutlet weak var imgProfileView               : UIImageView!
    @IBOutlet weak var menuTopView                  : UIView!
    @IBOutlet weak var btnSettings                  : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var menuItems                                   : NSArray = NSArray()
    var menuImages                                  : NSArray = NSArray()
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnSettingTapped(_ sender: UIButton) {
        let profileVC = AppStoryboard.Authentication.viewController(viewControllerClass: ProfileVC.self)
        profileVC.isOpenFromMenu = true
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: profileVC)
    }
    
    
    @IBAction func btnProfileTapped(_ sender: Any) {
        
        let profileVC = AppStoryboard.Authentication.viewController(viewControllerClass: ProfileVC.self)
        profileVC.isOpenFromMenu = true
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: profileVC)
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setUpView() {
        
        menuItems = [
            "Home",
            "Services",
            "Booking History",
            "Contact Us",
            "Notification",
            "Refer Friends",
            "About Us",
            "Help",
            "Logout"
        ]
        
        menuImages = [
            "imgIconHome",
            "imgIconService",
            "imgIconBooking",
            "imgIconContactUs",
            "imgIconNotification"
        ]
        
        self.menuTopView.backgroundColor = UIColor.menuTopBackground
        
        self.tblSideMenu.delegate = self
        self.tblSideMenu.dataSource = self
        self.tblSideMenu.reloadData()
        
        self.imgProfileView.layer.cornerRadius = imgProfileView.frame.height / 2
        self.imgProfileView.layer.borderColor = UIColor.white.cgColor
        self.imgProfileView.layer.borderWidth = 1.0
        self.lblName.text = "Peter Parker"
        self.lblPhoneNo.text = "+9198234678879"
        
    }
    
    //---------------------------------------------------------------------------
    
    func setDashBoardView()  {
        let dashboardVC = AppStoryboard.Dashboard.viewController(viewControllerClass: DashboardVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: dashboardVC)
    }
    
    //---------------------------------------------------------------------------
    
    func setAboutView()  {
        let aboutVC = AppStoryboard.AboutUs.viewController(viewControllerClass: AboutUsVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: aboutVC)
    }
    
    //---------------------------------------------------------------------------
    
    func setServiceView() {
        let serviceVC = AppStoryboard.Category.viewController(viewControllerClass: CategoryVC.self)
        serviceVC.isOpenFromMenu = true
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: serviceVC)
    }
    
    //---------------------------------------------------------------------------
    
    func setLegalView() {
        let legalVC = AppStoryboard.AboutUs.viewController(viewControllerClass: LegalVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: legalVC)
    }
    
    //---------------------------------------------------------------------------
    
    func setHelpView() {
        let helpVC = AppStoryboard.AboutUs.viewController(viewControllerClass: HelpVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: helpVC)
    }
    
    //---------------------------------------------------------------------------
    
    func setContactView() {
        let contactVC = AppStoryboard.ContactUs.viewController(viewControllerClass: ConatctUsVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: contactVC)
    }
    
    //---------------------------------------------------------------------------
    
    func setReferView() {
        let referVC = AppStoryboard.AboutUs.viewController(viewControllerClass: RefereVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: referVC)
    }
    
    //---------------------------------------------------------------------------
    
    func setNotificationView() {
        let NotificationVC = AppStoryboard.Notification.viewController(viewControllerClass: NotificationListVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: NotificationVC)
    }
    
    //---------------------------------------------------------------------------
    
    func setBookingHistoryView() {
        let bookingHistoryVC = AppStoryboard.Booking.viewController(viewControllerClass: BookingHistoryVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: bookingHistoryVC)
    }
    
    
    //---------------------------------------------------------------------------
    
    func loginConfirmation() {
        
        SJSwiftSideMenuController.hideLeftMenu()
        
        let actionOK = UIAlertAction(title: "Ok", style: .default) { (action) in
            let homeVC = AppStoryboard.Authentication.viewController(viewControllerClass: HomeTabVC.self)
            let navigationController = UINavigationController(rootViewController: homeVC)
            AppDelegate.sharedInstance.window?.rootViewController = navigationController
            UserDefaults.Account.set(false, forKey: UserDefaults.Account.BoolDefaultKey.isUserLoggedIn)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        self.showAlert(App.AppName, message: "Do you want to logout?", alertActions: [actionOK, actionCancel])
    }
    
    //---------------------------------------------------------------------------
    
    func tableViewCellSelected(at indexPath: IndexPath) {
        
        switch indexPath.row {
        
        // Home
        case 0 :
            self.setDashBoardView()
            break
            
        // Services
        case 1 :
            self.setServiceView()
            break
        
        // Book History
        case 2 :
            self.setBookingHistoryView()
            break
         
        // Contact Us
        case 3 :
            self.setContactView()
            break
           
        // Notification
        case 4 :
            self.setNotificationView()
            break
            
        // Refer Friend
        case 6 :
            self.setReferView()
            break
         
        // About Us
        case 7 :
            setAboutView()
            break
        
        // Help
        case 8 :
            self.setHelpView()
            break
        
        // Logout
        case 9 :
            self.loginConfirmation()
            break
        
        case 11 :
            self.setLegalView()
            break
            
        // Other Cell
        default :
            print("Other Cell Selected")
            break
        }
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let name = UserDefaults.UserData.object(forKey: .name) as? String {
            self.lblName.text = name
        }
        
        if let email = UserDefaults.UserData.object(forKey: .email) as? String {
            self.lblPhoneNo.text = email
        } else if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            self.lblPhoneNo.text = phone
        }
        
        if let strImageUrl = UserDefaults.UserData.object(forKey: .image) as? String {
            if let imageUrl = URL(string: strImageUrl) {
                self.imgProfileView.sd_setImage(with: imageUrl, completed: nil)
            }
        }
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension MenuVC: UITableViewDataSource
//---------------------------------------------------------------------------

extension MenuVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count + 3
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // This cell will seperator
        if indexPath.row == 5 || indexPath.row == 10 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SeperatorCell", for: indexPath)
            cell.isUserInteractionEnabled = false
            return cell
        }
        
        // This cell is last cell which display Legal and V 1.0
        if indexPath.row == 11 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LegalCell", for: indexPath)
            cell.isUserInteractionEnabled = true
            return cell
        }
        
        // Keep this code here, other wise it will throw error
        // This cell will set menu text and images
        let cell:MenuListCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuListCell
        
        // Based on condition text is setup to handle array index out of bound
        if indexPath.row > 5 && indexPath.row < 10 {
            cell.lblTitle.text = menuItems[indexPath.row - 1] as? String
        } else if indexPath.row < 6  {
            cell.lblTitle.text = menuItems[indexPath.row] as? String
        }
        
        // Setup image for top five cell
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 {
            cell.imgView.tintColor = .lightGray
            cell.imgView.image = UIImage(named: menuImages[indexPath.row] as! String)?.withRenderingMode(.alwaysTemplate)
        }
        
        return cell
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableViewCellSelected(at: indexPath)
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5 || indexPath.row == 10 {
            return 20
        } else if indexPath.row == 11 {
            return 30
        }
        return 47
    }
}



