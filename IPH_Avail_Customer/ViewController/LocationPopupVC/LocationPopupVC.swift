//
//  LocationPopupVC.swift
//  IPH_Avail_Customer
//
//  Created on 16/07/18.


import UIKit

class LocationPopupVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var labelUseThisLocation: UILabel!
    @IBOutlet weak var lblLatLong: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var ivStaticMap: UIImageView!
    @IBOutlet weak var btnSelectLocation: UIButton!
    @IBOutlet weak var btnChangeLocation: UIButton!
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnSelectLocationTapped(_ sender: UIButton) {
    
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnChangeLocationTapped(_ sender: UIButton) {
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
