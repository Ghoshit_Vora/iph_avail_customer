//
//  ProfileVC.swift
//  IPH_Avail_Vendor
//
//  Created on 04/07/18.


import UIKit

class ProfileVC: BaseVC {
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var topView                              : UIView!
    @IBOutlet weak var ivProfile                            : UIImageView!
    @IBOutlet weak var lblName                              : UILabel!
    @IBOutlet weak var lblMobileNumber                      : UILabel!
    @IBOutlet weak var tblProfile                           : UITableView!
    @IBOutlet weak var btnLogout                            : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var arrProfileData                                      : [[String: String]]?
    var isOpenFromMenu                                      : Bool = false
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        
        let actionOK = UIAlertAction(title: "Ok", style: .default) { (action) in
            let homeVC = AppStoryboard.Authentication.viewController(viewControllerClass: HomeTabVC.self)
            let navigationController = UINavigationController(rootViewController: homeVC)
            AppDelegate.sharedInstance.window?.rootViewController = navigationController
            UserDefaults.Account.set(false, forKey: UserDefaults.Account.BoolDefaultKey.isUserLoggedIn)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        self.showAlert(App.AppName, message: "Do you want to logout?", alertActions: [actionOK, actionCancel])
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setupData() {
        self.arrProfileData = [
            ["title": "My Profile", "image": "ic_my_profile"],
            ["title": "Booking History", "image": "imgIconBooking"],
            ["title": "Contact Us", "image": "ic_contact_us"]
        ]
    }
    
    //---------------------------------------------------------------------------
    
    func setupView() {
        
        self.setTitle()
        
        self.btnLogout.layer.cornerRadius = 10
        
        
        if let name = UserDefaults.UserData.object(forKey: .name) as? String {
            self.lblName.text = name
        }
        
        if let email = UserDefaults.UserData.object(forKey: .email) as? String {
            self.lblMobileNumber.text = email
        } else if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            self.lblMobileNumber.text = phone
        }
        
        if let strImageUrl = UserDefaults.UserData.object(forKey: .image) as? String {
            if let imageUrl = URL(string: strImageUrl) {
                self.ivProfile.sd_setImage(with: imageUrl, completed: nil)
            }
        }
        
        self.ivProfile.layer.cornerRadius = ivProfile.frame.height / 2
        self.ivProfile.layer.borderWidth = 10.0
        self.ivProfile.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
//        self.ivProfile.layer.masksToBounds = false
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = UIColor.black
        
        if isOpenFromMenu {
            self.setupLeftButtonWithTitle(isMenuRequired: true)
            self.isOpenFromMenu = false
        } else {
            self.setupLeftButtonWithTitle(isMenuRequired: false)
        }
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
        self.setupView()
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UITableViewDataSource Methods
//---------------------------------------------------------------------------

extension ProfileVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrProfileData != nil {
            return (self.arrProfileData?.count)!
        }
        return 0
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let profileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath)
        
        profileCell.textLabel?.text = self.arrProfileData![indexPath.row]["title"]
        profileCell.textLabel?.font = UIFont(name: "Calibri", size: 14)
        
        profileCell.imageView?.image = UIImage(named:self.arrProfileData![indexPath.row]["image"]!)
        
        return profileCell
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UITableViewDelegate Methods
//---------------------------------------------------------------------------

extension ProfileVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 2 {
            let vc = AppStoryboard.ContactUs.viewController(viewControllerClass: ConatctUsVC.self)
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}


