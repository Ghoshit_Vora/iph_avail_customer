//
//  RefereVC.swift
//  IPH_Avail_Customer
//
//  Created on 03/07/18.


import UIKit
import ObjectMapper
import AlamofireImage

class RefereVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var labelReferYourFriend                 : UILabel!
    @IBOutlet weak var ivReferFriend                        : UIImageView!
    @IBOutlet weak var labelShareVia                        : UILabel!
    @IBOutlet weak var btnWhatsAppShare                     : UIButton!
    @IBOutlet weak var btnFBShare                           : UIButton!
    @IBOutlet weak var btnGmailShare                        : UIButton!
    @IBOutlet weak var btnMore                              : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var shareData                                         : AVShareData!
    
    var noInternetVC : NoInternetConnectionVC?
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnWhatsAppShareTapped(_ sender: UIButton) {
        
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnFBShareTapped(_ sender: UIButton) {
        
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnGmailShareTapped(_ sender: UIButton) {
        
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnMoreTapped(_ sender: UIButton) {
        
        let strMessage = self.shareData != nil ? self.shareData.message! : "Hi! I'm Developer Team, I am enjoying services of Avail! Download the app <Download Url>"
        
        let activityVC = UIActivityViewController(activityItems: [strMessage], applicationActivities: nil)
        
        if #available(iOS 11.0, *) {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
                .markupAsPDF
            ]
        } else {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
            ]
        }
        
        AppDelegate.sharedInstance.window?.rootViewController?.present(activityVC, animated: true, completion: nil)
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setupView() {
        self.setTitle()
//        self.title = "Refer Friends"
        self.setupLeftButtonWithTitle(isMenuRequired: true)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = UIColor.black
    }
    
    //---------------------------------------------------------------------------
    
    func setup(shareData: [AVShareData]) {
        
        if let shareData = shareData.first {
            
            self.shareData = shareData
            
            if let strImg = shareData.image, let imageUrl = URL(string: strImg) {
                self.ivReferFriend.af_setImage(withURL: imageUrl)
            }
        }
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Web Service Call Methods
    //---------------------------------------------------------------------------
    
    func callWSToGetReferData(custId :String) {
        
        DataManager.sharedManager.getReferData(custID: custId, success: { (responseData, responseError) in
            
            if responseError == nil,
                let responseData = Mapper<AVReferRequest>().map(JSONObject: responseData),
                let shareData = responseData.shareData {
                
                self.setup(shareData: shareData)
            }
            
        }) { (error) in
            if error.localizedDescription == ErrorManager.noInternetConnectionError.errorDescription {
                self.noInternetVC = AppStoryboard.Authentication.viewController(viewControllerClass: NoInternetConnectionVC.self)
                AppDelegate.sharedInstance.window?.addSubview((self.noInternetVC?.view)!)
            } else {
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
        }
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        
        if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            self.callWSToGetReferData(custId: strCustomerId)
        } else {
            self.showAlertWithOKBtn(message: AppMessages.noDataFound)
        }
        
    }
}

extension RefereVC : NoInternetDelegate {
    func tryAgainClicked() {
        if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            self.callWSToGetReferData(custId: strCustomerId)
        } else {
            self.showAlertWithOKBtn(message: AppMessages.noDataFound)
        }
    }
}
