//
//  CategoryVC.swift
//  IPH_Avail_Customer

import UIKit
import ObjectMapper
import SDWebImage

class CategoryVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var tblCategory                  : UITableView!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var arrCategory                                 : [AVCategoryList] = []
    var isOpenFromMenu                              : Bool = false
    var noInternetVC : NoInternetConnectionVC?
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setUpView() {
        
        self.setUpNavigation(isImgNavigation: true, isAppColorRequired: false)
        
        if isOpenFromMenu {
            self.setUpLeftBarButton(isMenuRequired: true)
        } else {
            self.setUpLeftBarButton(isMenuRequired: false)
        }
        
        self.tblCategory.isHidden = true
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)
        
        if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            self.callCategoryWS(custId: strCustomerId)
        } else {
            self.showAlertWithOKBtn(message: AppMessages.noDataFound)
        }
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- WS Call Methods
    //---------------------------------------------------------------------------
    
    func callCategoryWS(custId : String) {
        
        DataManager.sharedManager.getCategoryData(custId: custId,success: { (response, error) in
            
            if error == nil, let responseData = Mapper<AVCategoryRequest>().map(JSONObject: response), let _ = responseData.message {
                
                self.arrCategory = responseData.categoryList!
                
                if self.arrCategory.count > 0 {
                    self.tblCategory.isHidden = false
                    self.tblCategory.reloadData()
                }
                
            }
        }, failure: { (error) in
            
            if error.localizedDescription == ErrorManager.noInternetConnectionError.errorDescription {
                self.noInternetVC = AppStoryboard.Authentication.viewController(viewControllerClass: NoInternetConnectionVC.self)
                AppDelegate.sharedInstance.window?.addSubview((self.noInternetVC?.view)!)
            } else {
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
        })
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //---------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        setUpView()
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension CategoryVC: UITableViewDelegate, UITableViewDataSource Methods
//---------------------------------------------------------------------------

extension CategoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCategory.count
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableCell.CategoryListCell) as! CategoryListCell
        
        let data = self.arrCategory[indexPath.row]
        
        cell.lblCategoryName.text = data.name
        let strUrl = URL(string: data.icon!)
        cell.imgCategory.sd_setImage(with: strUrl, placeholderImage: UIImage.init())
        
        return cell
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.arrCategory[indexPath.row]
        
        if data.subcat == "Yes" {
            let vc = AppStoryboard.Category.viewController(viewControllerClass: SubCategoryVC.self)
            vc.categoryId = data.catID!
            vc.categoryName = data.name!
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = AppStoryboard.Category.viewController(viewControllerClass: PersonListVC.self)
            vc.categoryId = data.catID!
            vc.categoryName = data.name!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

extension CategoryVC : NoInternetDelegate {
    func tryAgainClicked() {
        if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            self.callCategoryWS(custId: strCustomerId)
        } else {
            self.showAlertWithOKBtn(message: AppMessages.noDataFound)
        }
    }
}
