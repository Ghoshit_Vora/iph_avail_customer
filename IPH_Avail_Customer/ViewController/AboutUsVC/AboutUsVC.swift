//
//  AboutUsVC.swift
//  IPH_Avail_Customer
//
// Created on 21/05/18.


import UIKit
import ObjectMapper

class AboutUsVC: BaseVC {

    //MARK:-Properties
    
    @IBOutlet weak var imgAboutUs: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var const_webview_Height: NSLayoutConstraint!
    
    //MARK:-Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom Method
    
    func setupView() {
        
        self.setUpNavigation(isImgNavigation: true, isAppColorRequired: false)
        
        self.setUpLeftBarButton(isMenuRequired: true)
     
        self.webView.delegate = self
        setupData()
    }
    
    func setupData() {
        self.callAboutUsWS()
    }
    
    //MARK:-WS Method
    
    func callAboutUsWS() {
        
        DataManager.sharedManager.getAboutData(success: { (response, error) in
            
            if error == nil, let responseData = Mapper<AVAboutRequest>().map(JSONObject: response), let _ = responseData.message {
                
                if responseData.about!.count > 0 {
                    let aboutData = responseData.about![0]
                    
                    //self.lblAboutText.text = aboutData.text
                    self.webView.loadHTMLString(aboutData.text!, baseURL: nil)
                    //let strUrl = URL(string: aboutData.image!)
                }
                
            }
        }, failure: { (error) in
            
            self.showAlertWithOKBtn(message: error.localizedDescription)
        })
    }
    
    //MARK:-Action Method
    
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }

}

extension AboutUsVC:UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.frame.size = webView.sizeThatFits(.zero)
        webView.scrollView.isScrollEnabled=false;
        const_webview_Height.constant = webView.scrollView.contentSize.height
        webView.scalesPageToFit = true
    }
}
