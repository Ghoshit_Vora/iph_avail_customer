//
//  SearchPlacesVC.swift
//  IPH_Avail_Customer
//
//  Created on 12/06/18.


import UIKit
import GooglePlaces
import GoogleMaps

protocol SearchPlacesDelegate: class {
    func selected(place: String)
}

class SearchPlacesVC: UIViewController {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var tblPlacesList            : UITableView!
    @IBOutlet weak var searchBar                : UISearchBar!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    weak var delegate                           : SearchPlacesDelegate?
    var arrSearchedPlaces                       : [GMSPlace]?
    var searchController                        : UISearchController?
    var gmsPlacesClient                         : GMSPlacesClient?

    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    
    
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func searchPlaces() {
        searchController = UISearchController(searchResultsController: nil)
        searchController?.searchResultsUpdater = self
        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.dimsBackgroundDuringPresentation = false
        tblPlacesList.tableHeaderView = searchController?.searchBar
        
        gmsPlacesClient = GMSPlacesClient.shared()
        
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UISearchDisplayDelegate Methods
//---------------------------------------------------------------------------

extension SearchPlacesVC: UISearchDisplayDelegate {
    
    
    
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UISearchResultsUpdating Methods
//---------------------------------------------------------------------------

extension SearchPlacesVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if let searchText = searchController.searchBar.text, !searchText.isEmpty {
            
        }
    }
    
    
    
//    if let searchText = searchController.searchBar.text, !searchText.isEmpty {
//    filteredNFLTeams = unfilteredNFLTeams.filter { team in
//    return team.lowercased().contains(searchText.lowercased())
//    }
//
//    } else {
//    filteredNFLTeams = unfilteredNFLTeams
//    }
//    tableView.reloadData()
}
