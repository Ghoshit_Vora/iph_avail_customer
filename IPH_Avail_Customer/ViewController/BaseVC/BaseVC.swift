//
//  BaseVC.swift
//  IPH_Avail_Customer

import UIKit
import SJSwiftSideMenuController
import ObjectMapper
import CoreLocation

class BaseVC: UIViewController {
    
    //MARK:-Properties
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    static var currentPlace                     : CLLocationCoordinate2D?
    static var currentAddress                   : String?
    
    //MARK:-Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-Navigation Methods
    
    func setUpNavigation(isImgNavigation : Bool, isAppColorRequired : Bool) {
        
        if isImgNavigation {
            
//            let logo = UIImage(named: "imgNavigationLogo")
//            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
//            imageView.image = logo
////            imageView.contentMode = .scaleAspectFit
//            self.navigationItem.titleView = imageView
            self.setTitle()
            self.navigationController!.navigationBar.tintColor = UIColor.black
            
        } else {
            
            self.navigationController!.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
            self.navigationController?.navigationBar.barTintColor = Theme.AppMainPurpleColor
        }
    }
    
    func setUpLeftBarButton(isMenuRequired : Bool) {
        
        if isMenuRequired {
            let newBtn = UIBarButtonItem(image: UIImage(named: "imgIconMenu"), style: .plain, target: self, action: #selector(btnMenuTapped))
            
            self.navigationItem.leftBarButtonItem = newBtn
        } else {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
    }
    
    //---------------------------------------------------------------------------
    
    func removeBackButton() {
        let barButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = barButton
        self.navigationItem.leftBarButtonItem = barButton
        self.navigationItem.hidesBackButton = true
    }
    
    //---------------------------------------------------------------------------
    
    func setBackButton(isBlack: Bool) {

        if isBlack {
            let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_back_black"), style: .plain, target: self, action: #selector(backButtonTapped))
            self.navigationItem.backBarButtonItem = nil
            self.navigationItem.leftBarButtonItem = leftButton
            
        } else {
            let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_back_white"), style: .plain, target: self, action: #selector(backButtonTapped))
            self.navigationItem.backBarButtonItem = nil
            self.navigationItem.leftBarButtonItem = leftButton
        }
    }
    
    //---------------------------------------------------------------------------
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //---------------------------------------------------------------------------
    
    func setupLeftButtonWithTitle(isMenuRequired: Bool) {
        
        if isMenuRequired {
            
            let menuButton = UIBarButtonItem(image: UIImage(named: "imgIconMenu"), style: .plain, target: self, action: #selector(btnMenuTapped))
            
            let title = UIButton(type: .custom)
            title.setImage(#imageLiteral(resourceName: "imgNavigationLogo"), for: .normal)
            title.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//            let titleBarButton = UIBarButtonItem(customView: title)
            self.navigationItem.leftBarButtonItems = [menuButton]
            
        } else {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
    }
    
    //---------------------------------------------------------------------------
    
    func setupRightButton() {
        let btnUser = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_user"), style: .plain, target: self, action: #selector(btnUserTapped))
        self.navigationItem.rightBarButtonItem = btnUser
    }
    
    //---------------------------------------------------------------------------
    
    func setTitle() {
        
//        let titleLabel = UILabel()
//        let colour = Theme.AppMainPurpleColor
//        let attributes = [
//            NSAttributedStringKey.font as NSString: UIFont.systemFont(ofSize: 20),
//            NSAttributedStringKey.foregroundColor as NSString: colour,
//            NSAttributedStringKey.kern as NSString: 5.0 as AnyObject
//        ]
//        titleLabel.attributedText = NSAttributedString(string: "AVAIL", attributes: attributes as [NSAttributedStringKey : Any])
//        titleLabel.sizeToFit()
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "imgBigLogo"))
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.frame = CGRect(x: 0, y: 0, width: 60, height: 20)
        self.navigationItem.titleView = imageView
        self.navigationItem.titleView?.contentMode = .scaleAspectFit
        self.navigationItem.titleView?.frame.size = CGSize(width: 60, height: 20)
    }
    
    //---------------------------------------------------------------------------
    
    @objc func btnUserTapped() {
        let vc = AppStoryboard.Authentication.viewController(viewControllerClass: ProfileVC.self)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func btnMenuTapped() {
        
        SJSwiftSideMenuController.toggleLeftSideMenu()
    }
    
    
    //MARK:-Custom Alert Method

    func showAlertWithOKBtn(message:String) {
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        self.showAlert(App.AppName, message: message, alertActions: [okAction])
        
    }
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setTitle()
        // Do any additional setup after loading the view.
    }
}
