//
//  HelpVC.swift
//  IPH_Avail_Vendor
//
//  Created on 04/07/18.


import UIKit

class HelpVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var svHelp                           : UIScrollView!
    @IBOutlet weak var contentView                      : UIView!
    @IBOutlet weak var ivHelp                           : UIImageView!
    
    @IBOutlet weak var txtName                          : UITextField!
    @IBOutlet weak var txtEmail                         : UITextField!
    @IBOutlet weak var txtMobile                        : UITextField!
    @IBOutlet weak var tvMessage                        : UITextView!
    @IBOutlet weak var labelMessagePlaceholder          : UILabel!
    
    @IBOutlet weak var btnSubmit                        : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        let validator = Validator()
        validator.registerField(self.txtName, rule: [RequiredRule(message: "Please enter name")])
        validator.registerField(self.txtEmail, rule: [RequiredRule(message: "Please enter email address"), EmailRule(rulePattern: .regular, message: "Please enter valid email address")])
        validator.registerField(self.txtMobile, rule: [RequiredRule(message: "Please enter mobile number"), PhoneNumberRule(message: "Please enter valid mobile number") ])
        
        
        validator.validate({
            
            if self.tvMessage.text == "" || self.tvMessage.text == String(describing: CharacterSet.whitespacesAndNewlines) {
                self.showAlertWithMessage("Please enter mesaage")
            }
            
            self.callWSToSendHelpMessage()
        }) { (arrValidationError) in
            
            if let firstError = arrValidationError.first {
                self.showAlertWithMessage(firstError.errorMessage)
            }
        }
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Web Service Call Methods
    //---------------------------------------------------------------------------
    
    func callWSToSendHelpMessage() {
        
        let parameters = [
            "name": self.txtName.text!,
            "email": self.txtEmail.text!,
            "contact_no": self.txtMobile.text!,
            "message": self.tvMessage.text!,
            "app_type": "Iphone"
        ]
        
        DataManager.sharedManager.getHelpData(parameters: parameters, success: { (responseData, responseError) in
            
            if let response = responseData as? [String: Any] {
                if let msg = response["message"] as? String {
                    self.showAlertWithMessage(msg)
                }
            } else if let error = responseError as? [String: Any] {
                if let msg = error["message"] as? String {
                    self.showAlertWithMessage(msg)
                }
            }
            
        }) { (error) in
            self.showAlertWithMessage(error.localizedDescription)
        }
    }
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setupView() {
        self.setTitle()
        self.setupLeftButtonWithTitle(isMenuRequired: true)
        //self.setupRightButton()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = UIColor.black
        
        if let name = UserDefaults.UserData.object(forKey: .name) as? String {
            self.txtName.text = name
        }
        
        if let email = UserDefaults.UserData.object(forKey: .email) as? String {
            self.txtEmail.text = email
        }
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            self.txtMobile.text = phone
        }
    }
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
}


extension HelpVC: UITextFieldDelegate {
    
}

extension HelpVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        if changedText.count == 0 {
            self.labelMessagePlaceholder.isHidden = false
        } else {
            self.labelMessagePlaceholder.isHidden = true
        }
        
        return true
    }
}

