//
//  ForgotPasswordVC.swift
//  IPH_Avail_Customer

import UIKit

protocol ForgotPasswordDelegate: class {
    func submitClicked(emailNumber: String)
}

class ForgotPasswordVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var forgotPasswordView                   : UIView!
    @IBOutlet weak var lblForgotPasswordTitle               : UILabel!
    @IBOutlet weak var lblForgotPasswordDescription         : UILabel!
    @IBOutlet weak var txtEmail                             : UITextField!
    @IBOutlet weak var viewTextBottom                       : UIView!
    @IBOutlet weak var btnSubmit                            : UIButton!
    @IBOutlet weak var btnClose                             : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    weak var delegate                                       : ForgotPasswordDelegate?
    var noInternetVC                                : NoInternetConnectionVC?
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        let fieldValidator = Validator()
        
        fieldValidator.registerField(self.txtEmail, rule: [RequiredRule(message: "Please enter valid email"), EmailRule(message: "Please enter valid email")])
        
        fieldValidator.validate({
            self.callForgotWS()
        }) { validationError in
            // Failer
            if validationError.count > 0 {
                self.showAlertWithOKBtn(message: validationError[0].errorMessage)
            }
        }
        
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setupView() {
        self.lblForgotPasswordTitle.backgroundColor = Theme.AppMainPurpleColor
        self.btnSubmit.setTitleColor(Theme.AppMainPurpleColor, for: .normal)
        self.btnClose.setTitleColor(Theme.AppMainPurpleColor, for: .normal)
        
        // Set left icon in text field
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageView.clipsToBounds = true
        imageView.contentMode = .left
        //imageView.backgroundColor = .lightGray
        
        self.txtEmail.leftViewMode = .always
        self.txtEmail.leftView = imageView
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Call WS Methods
    //---------------------------------------------------------------------------
    
    func callForgotWS() {
        
        DataManager.sharedManager.setForgotData(email: self.txtEmail.text!, success: { (response, error) in
            if error == nil {
                self.showAlertWithOKBtn(message: "Successful send")
                
                self.view.removeFromSuperview()
            } else {
                self.showAlertWithOKBtn(message: error.debugDescription)
            }
        }) { (error) in
            
            if error.localizedDescription == ErrorManager.noInternetConnectionError.errorDescription {
                self.noInternetVC = AppStoryboard.Authentication.viewController(viewControllerClass: NoInternetConnectionVC.self)
                AppDelegate.sharedInstance.window?.addSubview((self.noInternetVC?.view)!)
            } else {
                self.showAlertWithOKBtn(message: "Something went wrong")
            }
        }
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
}
