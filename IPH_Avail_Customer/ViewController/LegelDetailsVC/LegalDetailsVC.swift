//
//  LegalDetailsVC.swift
//  IPH_Avail_Customer
//
// Created on 09/07/18.


import UIKit

class LegalDetailsVC: BaseVC {
    //MARK:-Properties
    
    @IBOutlet weak var lblDetail: UILabel!
    var lblText = String()
    
    //MARK:-Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblDetail.text = lblText
        self.title = lblText
    }

    

}
