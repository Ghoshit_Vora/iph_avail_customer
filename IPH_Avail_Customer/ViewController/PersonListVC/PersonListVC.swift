//
//  PersonListVC.swift
//  IPH_Avail_Customer

import UIKit
import ObjectMapper
import GooglePlaces
import GooglePlacePicker

class PersonListVC: BaseVC {

    //MARK:-Properties
    
    @IBOutlet weak var tblPersons: UITableView!
    
    @IBOutlet weak var labelVendorNearBy: UILabel!
    
    @IBOutlet weak var lblVendorNearBy: UILabel!
    
    @IBOutlet weak var btnChange: UIButton!
    
    var categoryName = String()
    var categoryId = String()
    var arrVender:[AVVendorList] = []
    var noInternetVC : NoInternetConnectionVC?
    var locationPopupVC : LocationPopupVC?
    
    //MARK:-Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView() {
        
        self.setUpNavigation(isImgNavigation: false, isAppColorRequired: true)
        
        self.removeBackButton()
        self.setBackButton(isBlack: false)
        
        self.setUpLeftBarButton(isMenuRequired: false)
        self.tblPersons.isHidden = true
        
        self.title = "Near By " + categoryName
        
        if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            
            self.callVendorListWS(custId: strCustomerId)
            
        } else {
            self.showAlertWithOKBtn(message: AppMessages.noDataFound)
        }
        
        self.lblVendorNearBy.text = BaseVC.currentAddress
        
    }
    
    //MARK:-Action Method
    
    
    @IBAction func btnChangeTapped(_ sender: UIButton) {
        
        let center = CLLocationCoordinate2D(latitude: (BaseVC.currentPlace?.latitude)!, longitude: (BaseVC.currentPlace?.longitude)!)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001,
                                               longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001,
                                               longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        
        let placePickerVC = GMSPlacePickerViewController(config: config)
        placePickerVC.delegate = self
        self.navigationController?.pushViewController(placePickerVC, animated: true)
    }
    
    //MARK:-WS Method
    
    func callVendorListWS(custId : String) {
        
        DataManager.sharedManager.getVendorData(catrgoryId: categoryId, CustomerId: custId, success: { (response, error) in
            
            if error == nil, let responseData = Mapper<AVVenderListRequest>().map(JSONObject: response), let _ = responseData.message {
                
                self.arrVender = responseData.vendorList!
                
                if self.arrVender.count > 0 {
                    self.tblPersons.isHidden = false
                    self.tblPersons.reloadData()
                }
                
            }
        }, failure: { (error) in
            
            if error.localizedDescription == ErrorManager.noInternetConnectionError.errorDescription {
                self.noInternetVC = AppStoryboard.Authentication.viewController(viewControllerClass: NoInternetConnectionVC.self)
                AppDelegate.sharedInstance.window?.addSubview((self.noInternetVC?.view)!)
            } else {
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
        })
    }
    
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
}

extension PersonListVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrVender.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableCell.PersonListCell) as! PersonListCell
        
        let data = self.arrVender[indexPath.row]
        
        cell.lblName.text = data.name
        let strUrl = URL(string: data.image!)
        cell.imgProfile.sd_setImage(with: strUrl, placeholderImage: UIImage.init())
        
        if let price = data.pricePerHour {
            cell.lblPrice.text = "$" + price
        } else {
            cell.lblPrice.text = "$0"
        }
        
        if let rating = data.ratting {
            cell.ratingView.rating = Double(rating)!
        } else {
            cell.ratingView.rating = 0.0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = AppStoryboard.Category.viewController(viewControllerClass: SelectVendorVC.self)
        
        vc.dictVendor = self.arrVender[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension PersonListVC : NoInternetDelegate {
    func tryAgainClicked() {
        if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            
            self.callVendorListWS(custId: strCustomerId)
            
        } else {
            self.showAlertWithOKBtn(message: AppMessages.noDataFound)
        }
    }
}


extension PersonListVC: GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        viewController.navigationController?.popViewController(animated: true)
        
//        locationPopupVC = AppStoryboard.Category.viewController(viewControllerClass: LocationPopupVC.self)
//
//
//        
//        if let address = place.formattedAddress {
//            locationPopupVC?.lblAddress.text = address
//        }
//
        let lat = place.coordinate.latitude
        let long = place.coordinate.longitude
        locationPopupVC?.lblLatLong.text = "\(lat), \(long)"
    }
    
    //---------------------------------------------------------------------------
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.navigationController?.popViewController(animated: true)
    }
}
