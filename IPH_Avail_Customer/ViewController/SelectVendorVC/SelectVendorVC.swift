//
//  SelectVendorVC.swift
//  IPH_Avail_Customer


import UIKit
import CoreLocation
import GoogleMaps
import Alamofire
import ObjectMapper

class SelectVendorVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var imgProfile                           : UIImageView!
    @IBOutlet weak var lblName                              : UILabel!
    @IBOutlet weak var lblPrice                             : UILabel!
    @IBOutlet weak var mapView                              : GMSMapView!
    @IBOutlet weak var viewCount                            : UIView!
    @IBOutlet weak var lblCount                             : UILabel!
    
    @IBOutlet weak var popupMainView                        : UIView!
    @IBOutlet weak var popupView                            : UIView!
    @IBOutlet weak var progressBarView                      : UIView!
    @IBOutlet weak var progressView                         : UIProgressView!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var dictVendor                                          : AVVendorList?
    var currentLocation                                     : CLLocationCoordinate2D?
    
    var timer                                               : Timer?
    var timerCount                                          : Double = 0.0
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setUpView()  {
        
        self.popupMainView.isHidden = true
        
        self.setUpNavigation(isImgNavigation: false, isAppColorRequired: true)
        
        self.setUpLeftBarButton(isMenuRequired: false)
        
        self.title = "Select Vendor"
        
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2
        
        viewCount.layer.cornerRadius = viewCount.frame.size.width / 2
        self.lblName.text = dictVendor?.name
        
        if let price = dictVendor?.pricePerHour {
            self.lblPrice.text = "$" + price
        } else {
            self.lblPrice.text = "$0"
        }
        
        let strUrl = URL(string: (dictVendor?.image!)!)
        self.imgProfile.sd_setImage(with: strUrl, placeholderImage: UIImage.init())
        
        // Set Map View Style
        if let strMapStyle = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
            do {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: strMapStyle)
            } catch {
                print("Error to set style = \(error.localizedDescription)")
            }
        }
        
        self.lblCount.text = dictVendor?.ratting
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnSelectTapped(_ sender: Any) {
        
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnFindAnotherTapped(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        
        self.popupMainView.isHidden = true
        self.popupView.alpha = 0.0
        
        self.showPopup()
    }
    
    //---------------------------------------------------------------------------
    
    func findDirection() {
        
        guard let strVendorLat = dictVendor?.lattitude,
            let strVendorLong = dictVendor?.longitude,
            let vendorLat = CLLocationDegrees(strVendorLat),
            let vendorLong = CLLocationDegrees(strVendorLong) else {
            return
        }
        
        if BaseVC.currentPlace != nil {
            
            let markerOrigin = GMSMarker()
            markerOrigin.position = CLLocationCoordinate2D(latitude: BaseVC.currentPlace!.latitude, longitude: BaseVC.currentPlace!.longitude)
            markerOrigin.map = self.mapView
            
            let markerDestination = GMSMarker()
            markerDestination.position = CLLocationCoordinate2D(latitude: vendorLat, longitude: vendorLong)
            markerDestination.map = self.mapView
            
            let destinationPlace = CLLocationCoordinate2D(latitude: vendorLat, longitude: vendorLong)
            
            // To draw a path need to enable some key which listed at below in library sections under googel maps API.
            // Those are Google Places API for iOS, Google Maps Roads API, Google Static Maps API, Google Street View Image API, Google Places API Web Service, Google Geocoding API, Google Maps Directions API, Google Maps Distance Matrix API, Google Maps Geolocation API, Google Maps Time Zone API
            
            let start = "\(BaseVC.currentPlace!.latitude),\(BaseVC.currentPlace!.longitude)"
            let end = "\(destinationPlace.latitude),\(destinationPlace.longitude)"
            
            
            // Create url to get all locations point which are available between path of Ahmedabad and Mehsana
            let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(start)&destination=\(end)&mode=driving&key=\(GoogleMap.APIKey)&sensor=true&traffic_mode=best_guess"
            
            Alamofire.request(url).responseJSON { response in
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)   // result of response serialization
                
                guard let httpResponse = response.response else {
                    return
                }
                
                if httpResponse.statusCode == 200 {
                    
                    if let routeData = response.data {
                        
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any]
                           
                            print(json!)
                            
//                            let routes = json!["routes"] as! [String: Any]
                            
//                            for route in routes
//                            {
//                                let routeOverviewPolyline = route["overview_polyline"] as! [String: Any]
//                                let points = routeOverviewPolyline?["points"]?.stringValue
//                                let path = GMSPath.init(fromEncodedPath: points!)
//                                let polyline = GMSPolyline.init(path: path)
//                                polyline.strokeWidth = 3.0
//                                polyline.strokeColor = UIColor.red
//                                polyline.map = self.mapView
                            
                                // Modified polyline style
//                                let styles = [GMSStrokeStyle.solidColor(.red)]
//                                let lengths: [NSNumber] = [1000, 1000, 1000, 1000]
//                                polyline.spans = GMSStyleSpans(polyline.path!, styles, lengths, .rhumb)
//                            }
                        } catch {
                            print("Error:- \(error.localizedDescription)")
                        }
                    }
                }
            }
        }
    }
    
    //---------------------------------------------------------------------------
    
    func showPopup() {
        
        UIView.animate(withDuration: 0.4, animations: {
            
            self.popupMainView.isHidden = false
            self.popupView.alpha = 1.0
            
        }) { (bool) in
            self.setupTimer()
        }
    }
    
    //---------------------------------------------------------------------------
    
    func setupTimer() {
        
//        UIView.animate(withDuration: 40, animations: {
//            self.progressView.setProgress(1.0, animated: true)
//        }) { (success) in
//            self.popupMainView.isHidden = true
//        }
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in

            self.timerCount += 0.1

            self.progressView.setProgress(Float(self.timerCount / 4), animated: true)
            
            print(self.timerCount)
            print(Float(self.timerCount / 4))

            if self.timerCount >= 4.0 {
                self.timer?.invalidate()
                self.timer = nil
                self.popupMainView.isHidden = true
            }
        })
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        findDirection()
    }
    
    //---------------------------------------------------------------------------
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
}
