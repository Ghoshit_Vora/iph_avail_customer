//
//  ConatctUsVC.swift
//  IPH_Avail_Customer

import UIKit
import ObjectMapper

struct ContactUs {
    
    var title: String
    var image: UIImage
    
    init(_ title: String, _ image: UIImage) {
        self.title = title
        self.image = image
    }
    
}

class ConatctUsVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var tblContactUs                         : UITableView!
    @IBOutlet weak var btnCallNow                           : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var arrContactUs                                        : [ContactUs]?
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnCallNowTapped(_ sender: UIButton) {
        
        let strNumber = self.arrContactUs![2].title.components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
        let strUrl = "tel://" + strNumber
        
            if let url = URL(string: strUrl), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        
        
    }
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Web Service Call Methods
    //---------------------------------------------------------------------------
    
    func getContactData() {
        
        DataManager.sharedManager.getContactData(success: { (responseData, responseError) in
            
            if responseError == nil,
                let responseData = Mapper<ContactData>().map(JSONObject: responseData),
                let contact = responseData.contact {
                
                self.arrContactUs = [ContactUs]()
                
                if let name = contact.address1 {
                    let contact = ContactUs(name, #imageLiteral(resourceName: "icon_address"))
                    self.arrContactUs?.append(contact)
                }
                
                if let website = contact.website {
                    let contact = ContactUs(website, #imageLiteral(resourceName: "icon_website"))
                    self.arrContactUs?.append(contact)
                }
                
                if let phone = contact.phone {
                    let contact = ContactUs(phone, #imageLiteral(resourceName: "icon_mobile"))
                    self.arrContactUs?.append(contact)
                }
                
                if let email = contact.email {
                    let contact = ContactUs(email, #imageLiteral(resourceName: "icon_mail"))
                    self.arrContactUs?.append(contact)
                }
                
                self.tblContactUs.reloadData()
                
            }
            
        }) { (error) in
            self.showAlert(App.AppName, message: error.localizedDescription)
        }
    }
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setupView() {
        self.setTitle()
        self.setupLeftButtonWithTitle(isMenuRequired: true)
        //self.setupRightButton()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = UIColor.black
    }
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.getContactData()
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UITableViewDataSource Methods
//---------------------------------------------------------------------------

extension ConatctUsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrContactUs != nil {
            return (self.arrContactUs?.count)!
        }
        return 0
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let profileCell = tableView.dequeueReusableCell(withIdentifier: "ContactUsCell", for: indexPath)
        
        profileCell.textLabel?.text = self.arrContactUs![indexPath.row].title
        profileCell.imageView?.image = self.arrContactUs![indexPath.row].image
        
        return profileCell
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UITableViewDelegate Methods
//---------------------------------------------------------------------------

extension ConatctUsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

