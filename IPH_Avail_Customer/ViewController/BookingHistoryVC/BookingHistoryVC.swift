//
//  BookingHistoryVC.swift
//  IPH_Avail_Customer
//
//  Created on 17/07/18.


import UIKit
import ObjectMapper

class BookingHistoryVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var tblBookingHistory: UITableView!
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var arrBookingHistory:[AVBookingList] = []
    var noInternetVC : NoInternetConnectionVC?

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    
    
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setUpData() {
        
        self.tblBookingHistory.isHidden = true
        self.setTitle()
        self.setupLeftButtonWithTitle(isMenuRequired: true)
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    
    func callBookingHistoryWS(custID : String) {
        
        DataManager.sharedManager.getBookingHistoryData(custId: custID, pageNo: "0", success: { (response, error) in
            
            if error == nil, let responseData = Mapper<AVBookingHistoryRequest>().map(JSONObject: response), let _ = responseData.message {
                
                self.arrBookingHistory = responseData.bookingList!
                
                if self.arrBookingHistory.count > 0 {
                    self.tblBookingHistory.isHidden = false
                    self.tblBookingHistory.reloadData()
                }
                
            }
        }, failure: { (error) in
            
            if error.localizedDescription == ErrorManager.noInternetConnectionError.errorDescription {
                self.noInternetVC = AppStoryboard.Authentication.viewController(viewControllerClass: NoInternetConnectionVC.self)
                AppDelegate.sharedInstance.window?.addSubview((self.noInternetVC?.view)!)
            } else {
                self.showAlertWithOKBtn(message: error.localizedDescription)
            }
        })
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            self.callBookingHistoryWS(custID: strCustomerId)
        } else {
            self.showAlertWithOKBtn(message: AppMessages.noDataFound)
        }
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UITableViewDataSource Methods
//---------------------------------------------------------------------------

extension BookingHistoryVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBookingHistory.count
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingHistoryTVC", for: indexPath) as! BookingHistoryTVC
        
        let bookingData = self.arrBookingHistory[indexPath.row]
        
        cell.lblName.text = bookingData.vendorName
        cell.lblBookingNumber.text = "#" + bookingData.iD!
        cell.lblStatus.text = bookingData.vendorDetail
        cell.lblPrice.text = "$" + bookingData.amount!
        cell.lblBookingStatus.text = bookingData.status
        
        cell.lblDateTime.text = bookingData.bookDate! + " " + bookingData.bookTime!
        
        if let strUrl = URL(string: bookingData.vendorImage!) {
            cell.ivBookingProfile.sd_setImage(with: strUrl, placeholderImage: UIImage.init())
        }
        
        
        return cell
    }
}


//---------------------------------------------------------------------------
// MARK:-
// MARK:- Extension - UITableViewDelegate Methods
//---------------------------------------------------------------------------

extension BookingHistoryVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension BookingHistoryVC : NoInternetDelegate {
    func tryAgainClicked() {
        
        if let strCustomerId =  UserDefaults.UserData.object(forKey: UserDefaults.UserData.ObjectDefaultKey.userId) as? String {
            self.callBookingHistoryWS(custID: strCustomerId)
        } else {
            self.showAlertWithOKBtn(message: AppMessages.noDataFound)
        }
    }
}

