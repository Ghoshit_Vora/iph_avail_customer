//
//  AVBookingList.swift
//
//  Created on 18/07/18


import Foundation
import ObjectMapper

public final class AVBookingList: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userAddress = "user_address"
    static let otp = "otp"
    static let amount = "amount"
    static let category = "category"
    static let iD = "ID"
    static let bookDate = "book_date"
    static let vendorLongitude = "vendor_longitude"
    static let userLongitude = "user_longitude"
    static let totalHour = "total_hour"
    static let paymentStatus = "payment_status"
    static let vendorImage = "vendor_image"
    static let serviceStartTime = "service_start_time"
    static let userLattitude = "user_lattitude"
    static let vendorRattings = "vendor_rattings"
    static let bookTime = "book_time"
    static let bookingID = "bookingID"
    static let status = "status"
    static let vendorName = "vendor_name"
    static let vendorAddress = "vendor_address"
    static let vendorPhone = "vendor_phone"
    static let vendorDetail = "vendor_detail"
    static let vendorID = "vendorID"
    static let vendorRate = "vendor_rate"
    static let vendorLattitude = "vendor_lattitude"
    static let historyList = "history_list"
  }

  // MARK: Properties
  public var userAddress: String?
  public var otp: String?
  public var amount: String?
  public var category: String?
  public var iD: String?
  public var bookDate: String?
  public var vendorLongitude: String?
  public var userLongitude: String?
  public var totalHour: String?
  public var paymentStatus: String?
  public var vendorImage: String?
  public var serviceStartTime: String?
  public var userLattitude: String?
  public var vendorRattings: String?
  public var bookTime: String?
  public var bookingID: String?
  public var status: String?
  public var vendorName: String?
  public var vendorAddress: String?
  public var vendorPhone: String?
  public var vendorDetail: String?
  public var vendorID: String?
  public var vendorRate: String?
  public var vendorLattitude: String?
  public var historyList: [AVHistoryList]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    userAddress <- map[SerializationKeys.userAddress]
    otp <- map[SerializationKeys.otp]
    amount <- map[SerializationKeys.amount]
    category <- map[SerializationKeys.category]
    iD <- map[SerializationKeys.iD]
    bookDate <- map[SerializationKeys.bookDate]
    vendorLongitude <- map[SerializationKeys.vendorLongitude]
    userLongitude <- map[SerializationKeys.userLongitude]
    totalHour <- map[SerializationKeys.totalHour]
    paymentStatus <- map[SerializationKeys.paymentStatus]
    vendorImage <- map[SerializationKeys.vendorImage]
    serviceStartTime <- map[SerializationKeys.serviceStartTime]
    userLattitude <- map[SerializationKeys.userLattitude]
    vendorRattings <- map[SerializationKeys.vendorRattings]
    bookTime <- map[SerializationKeys.bookTime]
    bookingID <- map[SerializationKeys.bookingID]
    status <- map[SerializationKeys.status]
    vendorName <- map[SerializationKeys.vendorName]
    vendorAddress <- map[SerializationKeys.vendorAddress]
    vendorPhone <- map[SerializationKeys.vendorPhone]
    vendorDetail <- map[SerializationKeys.vendorDetail]
    vendorID <- map[SerializationKeys.vendorID]
    vendorRate <- map[SerializationKeys.vendorRate]
    vendorLattitude <- map[SerializationKeys.vendorLattitude]
    historyList <- map[SerializationKeys.historyList]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userAddress { dictionary[SerializationKeys.userAddress] = value }
    if let value = otp { dictionary[SerializationKeys.otp] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value }
    if let value = category { dictionary[SerializationKeys.category] = value }
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = bookDate { dictionary[SerializationKeys.bookDate] = value }
    if let value = vendorLongitude { dictionary[SerializationKeys.vendorLongitude] = value }
    if let value = userLongitude { dictionary[SerializationKeys.userLongitude] = value }
    if let value = totalHour { dictionary[SerializationKeys.totalHour] = value }
    if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
    if let value = vendorImage { dictionary[SerializationKeys.vendorImage] = value }
    if let value = serviceStartTime { dictionary[SerializationKeys.serviceStartTime] = value }
    if let value = userLattitude { dictionary[SerializationKeys.userLattitude] = value }
    if let value = vendorRattings { dictionary[SerializationKeys.vendorRattings] = value }
    if let value = bookTime { dictionary[SerializationKeys.bookTime] = value }
    if let value = bookingID { dictionary[SerializationKeys.bookingID] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = vendorName { dictionary[SerializationKeys.vendorName] = value }
    if let value = vendorAddress { dictionary[SerializationKeys.vendorAddress] = value }
    if let value = vendorPhone { dictionary[SerializationKeys.vendorPhone] = value }
    if let value = vendorDetail { dictionary[SerializationKeys.vendorDetail] = value }
    if let value = vendorID { dictionary[SerializationKeys.vendorID] = value }
    if let value = vendorRate { dictionary[SerializationKeys.vendorRate] = value }
    if let value = vendorLattitude { dictionary[SerializationKeys.vendorLattitude] = value }
    if let value = historyList { dictionary[SerializationKeys.historyList] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.userAddress = aDecoder.decodeObject(forKey: SerializationKeys.userAddress) as? String
    self.otp = aDecoder.decodeObject(forKey: SerializationKeys.otp) as? String
    self.amount = aDecoder.decodeObject(forKey: SerializationKeys.amount) as? String
    self.category = aDecoder.decodeObject(forKey: SerializationKeys.category) as? String
    self.iD = aDecoder.decodeObject(forKey: SerializationKeys.iD) as? String
    self.bookDate = aDecoder.decodeObject(forKey: SerializationKeys.bookDate) as? String
    self.vendorLongitude = aDecoder.decodeObject(forKey: SerializationKeys.vendorLongitude) as? String
    self.userLongitude = aDecoder.decodeObject(forKey: SerializationKeys.userLongitude) as? String
    self.totalHour = aDecoder.decodeObject(forKey: SerializationKeys.totalHour) as? String
    self.paymentStatus = aDecoder.decodeObject(forKey: SerializationKeys.paymentStatus) as? String
    self.vendorImage = aDecoder.decodeObject(forKey: SerializationKeys.vendorImage) as? String
    self.serviceStartTime = aDecoder.decodeObject(forKey: SerializationKeys.serviceStartTime) as? String
    self.userLattitude = aDecoder.decodeObject(forKey: SerializationKeys.userLattitude) as? String
    self.vendorRattings = aDecoder.decodeObject(forKey: SerializationKeys.vendorRattings) as? String
    self.bookTime = aDecoder.decodeObject(forKey: SerializationKeys.bookTime) as? String
    self.bookingID = aDecoder.decodeObject(forKey: SerializationKeys.bookingID) as? String
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.vendorName = aDecoder.decodeObject(forKey: SerializationKeys.vendorName) as? String
    self.vendorAddress = aDecoder.decodeObject(forKey: SerializationKeys.vendorAddress) as? String
    self.vendorPhone = aDecoder.decodeObject(forKey: SerializationKeys.vendorPhone) as? String
    self.vendorDetail = aDecoder.decodeObject(forKey: SerializationKeys.vendorDetail) as? String
    self.vendorID = aDecoder.decodeObject(forKey: SerializationKeys.vendorID) as? String
    self.vendorRate = aDecoder.decodeObject(forKey: SerializationKeys.vendorRate) as? String
    self.vendorLattitude = aDecoder.decodeObject(forKey: SerializationKeys.vendorLattitude) as? String
    self.historyList = aDecoder.decodeObject(forKey: SerializationKeys.historyList) as? [AVHistoryList]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(userAddress, forKey: SerializationKeys.userAddress)
    aCoder.encode(otp, forKey: SerializationKeys.otp)
    aCoder.encode(amount, forKey: SerializationKeys.amount)
    aCoder.encode(category, forKey: SerializationKeys.category)
    aCoder.encode(iD, forKey: SerializationKeys.iD)
    aCoder.encode(bookDate, forKey: SerializationKeys.bookDate)
    aCoder.encode(vendorLongitude, forKey: SerializationKeys.vendorLongitude)
    aCoder.encode(userLongitude, forKey: SerializationKeys.userLongitude)
    aCoder.encode(totalHour, forKey: SerializationKeys.totalHour)
    aCoder.encode(paymentStatus, forKey: SerializationKeys.paymentStatus)
    aCoder.encode(vendorImage, forKey: SerializationKeys.vendorImage)
    aCoder.encode(serviceStartTime, forKey: SerializationKeys.serviceStartTime)
    aCoder.encode(userLattitude, forKey: SerializationKeys.userLattitude)
    aCoder.encode(vendorRattings, forKey: SerializationKeys.vendorRattings)
    aCoder.encode(bookTime, forKey: SerializationKeys.bookTime)
    aCoder.encode(bookingID, forKey: SerializationKeys.bookingID)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(vendorName, forKey: SerializationKeys.vendorName)
    aCoder.encode(vendorAddress, forKey: SerializationKeys.vendorAddress)
    aCoder.encode(vendorPhone, forKey: SerializationKeys.vendorPhone)
    aCoder.encode(vendorDetail, forKey: SerializationKeys.vendorDetail)
    aCoder.encode(vendorID, forKey: SerializationKeys.vendorID)
    aCoder.encode(vendorRate, forKey: SerializationKeys.vendorRate)
    aCoder.encode(vendorLattitude, forKey: SerializationKeys.vendorLattitude)
    aCoder.encode(historyList, forKey: SerializationKeys.historyList)
  }

}
