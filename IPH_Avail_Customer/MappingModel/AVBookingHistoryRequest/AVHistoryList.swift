//
//  AVHistoryList.swift
//
//  Created on 18/07/18


import Foundation
import ObjectMapper

public final class AVHistoryList: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let time = "time"
    static let title = "title"
    static let subTitle = "sub_title"
  }

  // MARK: Properties
  public var time: String?
  public var title: String?
  public var subTitle: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    time <- map[SerializationKeys.time]
    title <- map[SerializationKeys.title]
    subTitle <- map[SerializationKeys.subTitle]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = time { dictionary[SerializationKeys.time] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = subTitle { dictionary[SerializationKeys.subTitle] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.time = aDecoder.decodeObject(forKey: SerializationKeys.time) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.subTitle = aDecoder.decodeObject(forKey: SerializationKeys.subTitle) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(time, forKey: SerializationKeys.time)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(subTitle, forKey: SerializationKeys.subTitle)
  }

}
