//
//  AVBookingHistoryRequest.swift
//
//  Created on 18/07/18


import Foundation
import ObjectMapper

public final class AVBookingHistoryRequest: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bookingList = "booking_list"
    static let msgcode = "msgcode"
    static let message = "message"
  }

  // MARK: Properties
  public var bookingList: [AVBookingList]?
  public var msgcode: String?
  public var message: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    bookingList <- map[SerializationKeys.bookingList]
    msgcode <- map[SerializationKeys.msgcode]
    message <- map[SerializationKeys.message]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = bookingList { dictionary[SerializationKeys.bookingList] = value.map { $0.dictionaryRepresentation() } }
    if let value = msgcode { dictionary[SerializationKeys.msgcode] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.bookingList = aDecoder.decodeObject(forKey: SerializationKeys.bookingList) as? [AVBookingList]
    self.msgcode = aDecoder.decodeObject(forKey: SerializationKeys.msgcode) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(bookingList, forKey: SerializationKeys.bookingList)
    aCoder.encode(msgcode, forKey: SerializationKeys.msgcode)
    aCoder.encode(message, forKey: SerializationKeys.message)
  }

}
