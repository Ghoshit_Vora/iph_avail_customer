//
//  AVShareData.swift
//
//  Created on 05/07/18


import Foundation
import ObjectMapper

public final class AVShareData: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let shareImage = "share_image"
    static let message = "message"
    static let image = "image"
    static let shareMessage = "share_message"
  }

  // MARK: Properties
  public var shareImage: String?
  public var message: String?
  public var image: String?
  public var shareMessage: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    shareImage <- map[SerializationKeys.shareImage]
    message <- map[SerializationKeys.message]
    image <- map[SerializationKeys.image]
    shareMessage <- map[SerializationKeys.shareMessage]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = shareImage { dictionary[SerializationKeys.shareImage] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = shareMessage { dictionary[SerializationKeys.shareMessage] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.shareImage = aDecoder.decodeObject(forKey: SerializationKeys.shareImage) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.shareMessage = aDecoder.decodeObject(forKey: SerializationKeys.shareMessage) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(shareImage, forKey: SerializationKeys.shareImage)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(shareMessage, forKey: SerializationKeys.shareMessage)
  }

}
