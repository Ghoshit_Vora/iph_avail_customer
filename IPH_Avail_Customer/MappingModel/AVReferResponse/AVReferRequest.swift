//
//  AVReferRequest.swift
//
//  Created on 05/07/18


import Foundation
import ObjectMapper

public final class AVReferRequest: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let shareData = "share_data"
    static let msgcode = "msgcode"
    static let message = "message"
  }

  // MARK: Properties
  public var shareData: [AVShareData]?
  public var msgcode: String?
  public var message: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    shareData <- map[SerializationKeys.shareData]
    msgcode <- map[SerializationKeys.msgcode]
    message <- map[SerializationKeys.message]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = shareData { dictionary[SerializationKeys.shareData] = value.map { $0.dictionaryRepresentation() } }
    if let value = msgcode { dictionary[SerializationKeys.msgcode] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.shareData = aDecoder.decodeObject(forKey: SerializationKeys.shareData) as? [AVShareData]
    self.msgcode = aDecoder.decodeObject(forKey: SerializationKeys.msgcode) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(shareData, forKey: SerializationKeys.shareData)
    aCoder.encode(msgcode, forKey: SerializationKeys.msgcode)
    aCoder.encode(message, forKey: SerializationKeys.message)
  }

}
