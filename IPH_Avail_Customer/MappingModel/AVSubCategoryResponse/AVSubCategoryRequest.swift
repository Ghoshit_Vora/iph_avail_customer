//
//  AVSubCategoryRequest.swift
//
// Created on 06/07/18


import Foundation
import ObjectMapper

public final class AVSubCategoryRequest: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let message = "message"
    static let msgcode = "msgcode"
    static let subCategoryList = "sub_category_list"
  }

  // MARK: Properties
  public var message: String?
  public var msgcode: String?
  public var subCategoryList: [AVSubCategoryList]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    message <- map[SerializationKeys.message]
    msgcode <- map[SerializationKeys.msgcode]
    subCategoryList <- map[SerializationKeys.subCategoryList]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = msgcode { dictionary[SerializationKeys.msgcode] = value }
    if let value = subCategoryList { dictionary[SerializationKeys.subCategoryList] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.msgcode = aDecoder.decodeObject(forKey: SerializationKeys.msgcode) as? String
    self.subCategoryList = aDecoder.decodeObject(forKey: SerializationKeys.subCategoryList) as? [AVSubCategoryList]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(msgcode, forKey: SerializationKeys.msgcode)
    aCoder.encode(subCategoryList, forKey: SerializationKeys.subCategoryList)
  }

}
