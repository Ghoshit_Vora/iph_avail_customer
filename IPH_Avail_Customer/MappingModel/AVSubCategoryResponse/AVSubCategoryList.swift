//
//  AVSubCategoryList.swift
//
// Created on 06/07/18


import Foundation
import ObjectMapper

public final class AVSubCategoryList: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let catID = "catID"
    static let icon = "icon"
    static let name = "name"
    static let subcat = "subcat"
    static let sr = "sr"
    static let image = "image"
  }

  // MARK: Properties
  public var catID: String?
  public var icon: String?
  public var name: String?
  public var subcat: String?
  public var sr: String?
  public var image: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    catID <- map[SerializationKeys.catID]
    icon <- map[SerializationKeys.icon]
    name <- map[SerializationKeys.name]
    subcat <- map[SerializationKeys.subcat]
    sr <- map[SerializationKeys.sr]
    image <- map[SerializationKeys.image]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = catID { dictionary[SerializationKeys.catID] = value }
    if let value = icon { dictionary[SerializationKeys.icon] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = subcat { dictionary[SerializationKeys.subcat] = value }
    if let value = sr { dictionary[SerializationKeys.sr] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.catID = aDecoder.decodeObject(forKey: SerializationKeys.catID) as? String
    self.icon = aDecoder.decodeObject(forKey: SerializationKeys.icon) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.subcat = aDecoder.decodeObject(forKey: SerializationKeys.subcat) as? String
    self.sr = aDecoder.decodeObject(forKey: SerializationKeys.sr) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(catID, forKey: SerializationKeys.catID)
    aCoder.encode(icon, forKey: SerializationKeys.icon)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(subcat, forKey: SerializationKeys.subcat)
    aCoder.encode(sr, forKey: SerializationKeys.sr)
    aCoder.encode(image, forKey: SerializationKeys.image)
  }

}
