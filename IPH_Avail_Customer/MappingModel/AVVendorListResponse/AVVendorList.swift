//
//  AVVendorList.swift
//
//  Created on 02/06/18


import Foundation
import ObjectMapper

public final class AVVendorList: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let city = "city"
    static let name = "name"
    static let shortDesc = "short_desc"
    static let email = "email"
    static let image = "image"
    static let ratting = "ratting"
    static let phone = "phone"
    static let vendorID = "vendorID"
    static let lattitude = "lattitude"
    static let pricePerHour = "price_per_hour"
    static let longitude = "longitude"
    static let zipcode = "zipcode"
  }

  // MARK: Properties
  public var city: String?
  public var name: String?
  public var shortDesc: String?
  public var email: String?
  public var image: String?
  public var ratting: String?
  public var phone: String?
  public var vendorID: String?
  public var lattitude: String?
  public var pricePerHour: String?
  public var longitude: String?
  public var zipcode: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    city <- map[SerializationKeys.city]
    name <- map[SerializationKeys.name]
    shortDesc <- map[SerializationKeys.shortDesc]
    email <- map[SerializationKeys.email]
    image <- map[SerializationKeys.image]
    ratting <- map[SerializationKeys.ratting]
    phone <- map[SerializationKeys.phone]
    vendorID <- map[SerializationKeys.vendorID]
    lattitude <- map[SerializationKeys.lattitude]
    pricePerHour <- map[SerializationKeys.pricePerHour]
    longitude <- map[SerializationKeys.longitude]
    zipcode <- map[SerializationKeys.zipcode]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = city { dictionary[SerializationKeys.city] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = shortDesc { dictionary[SerializationKeys.shortDesc] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = ratting { dictionary[SerializationKeys.ratting] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = vendorID { dictionary[SerializationKeys.vendorID] = value }
    if let value = lattitude { dictionary[SerializationKeys.lattitude] = value }
    if let value = pricePerHour { dictionary[SerializationKeys.pricePerHour] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = zipcode { dictionary[SerializationKeys.zipcode] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.shortDesc = aDecoder.decodeObject(forKey: SerializationKeys.shortDesc) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.ratting = aDecoder.decodeObject(forKey: SerializationKeys.ratting) as? String
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.vendorID = aDecoder.decodeObject(forKey: SerializationKeys.vendorID) as? String
    self.lattitude = aDecoder.decodeObject(forKey: SerializationKeys.lattitude) as? String
    self.pricePerHour = aDecoder.decodeObject(forKey: SerializationKeys.pricePerHour) as? String
    self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? String
    self.zipcode = aDecoder.decodeObject(forKey: SerializationKeys.zipcode) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(city, forKey: SerializationKeys.city)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(shortDesc, forKey: SerializationKeys.shortDesc)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(ratting, forKey: SerializationKeys.ratting)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(vendorID, forKey: SerializationKeys.vendorID)
    aCoder.encode(lattitude, forKey: SerializationKeys.lattitude)
    aCoder.encode(pricePerHour, forKey: SerializationKeys.pricePerHour)
    aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    aCoder.encode(zipcode, forKey: SerializationKeys.zipcode)
  }

}
