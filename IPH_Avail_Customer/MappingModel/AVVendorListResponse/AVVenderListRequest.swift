//
//  AVVenderListRequest.swift
//
//  Created on 02/06/18


import Foundation
import ObjectMapper

public final class AVVenderListRequest: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let msgcode = "msgcode"
    static let message = "message"
    static let image = "image"
    static let vendorList = "vendor_list"
  }

  // MARK: Properties
  public var msgcode: String?
  public var message: String?
  public var image: String?
  public var vendorList: [AVVendorList]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    msgcode <- map[SerializationKeys.msgcode]
    message <- map[SerializationKeys.message]
    image <- map[SerializationKeys.image]
    vendorList <- map[SerializationKeys.vendorList]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = msgcode { dictionary[SerializationKeys.msgcode] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = vendorList { dictionary[SerializationKeys.vendorList] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.msgcode = aDecoder.decodeObject(forKey: SerializationKeys.msgcode) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.vendorList = aDecoder.decodeObject(forKey: SerializationKeys.vendorList) as? [AVVendorList]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(msgcode, forKey: SerializationKeys.msgcode)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(vendorList, forKey: SerializationKeys.vendorList)
  }

}
