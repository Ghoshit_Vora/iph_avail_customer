//
//  AVDashboardRequest.swift
//
//  Created on 21/05/18


import Foundation
import ObjectMapper

public final class AVDashboardRequest: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let version = "version"
    static let msgcode = "msgcode"
    static let message = "message"
    static let bannerList = "banner_list"
    static let categoryHomeList = "category_home_list"
    static let msg = "msg"
  }

  // MARK: Properties
  public var version: String?
  public var msgcode: String?
  public var message: String?
  public var bannerList: [AVBannerList]?
  public var categoryHomeList: [AVCategoryHomeList]?
  public var msg: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    version <- map[SerializationKeys.version]
    msgcode <- map[SerializationKeys.msgcode]
    message <- map[SerializationKeys.message]
    bannerList <- map[SerializationKeys.bannerList]
    categoryHomeList <- map[SerializationKeys.categoryHomeList]
    msg <- map[SerializationKeys.msg]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = version { dictionary[SerializationKeys.version] = value }
    if let value = msgcode { dictionary[SerializationKeys.msgcode] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = bannerList { dictionary[SerializationKeys.bannerList] = value.map { $0.dictionaryRepresentation() } }
    if let value = categoryHomeList { dictionary[SerializationKeys.categoryHomeList] = value.map { $0.dictionaryRepresentation() } }
    if let value = msg { dictionary[SerializationKeys.msg] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.version = aDecoder.decodeObject(forKey: SerializationKeys.version) as? String
    self.msgcode = aDecoder.decodeObject(forKey: SerializationKeys.msgcode) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.bannerList = aDecoder.decodeObject(forKey: SerializationKeys.bannerList) as? [AVBannerList]
    self.categoryHomeList = aDecoder.decodeObject(forKey: SerializationKeys.categoryHomeList) as? [AVCategoryHomeList]
    self.msg = aDecoder.decodeObject(forKey: SerializationKeys.msg) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(version, forKey: SerializationKeys.version)
    aCoder.encode(msgcode, forKey: SerializationKeys.msgcode)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(bannerList, forKey: SerializationKeys.bannerList)
    aCoder.encode(categoryHomeList, forKey: SerializationKeys.categoryHomeList)
    aCoder.encode(msg, forKey: SerializationKeys.msg)
  }

}
