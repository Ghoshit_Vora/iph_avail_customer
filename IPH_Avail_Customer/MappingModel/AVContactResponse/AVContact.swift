//
//  AVContact.swift
//
//  Created on 05/07/18


import Foundation
import ObjectMapper

public final class AVContact: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let phone = "phone"
    static let address1 = "address_1"
    static let email = "email"
    static let call = "call"
    static let message = "message"
    static let website = "website"
  }

  // MARK: Properties
  public var phone: String?
  public var address1: String?
  public var email: String?
  public var call: String?
  public var message: String?
  public var website: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    phone <- map[SerializationKeys.phone]
    address1 <- map[SerializationKeys.address1]
    email <- map[SerializationKeys.email]
    call <- map[SerializationKeys.call]
    message <- map[SerializationKeys.message]
    website <- map[SerializationKeys.website]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = address1 { dictionary[SerializationKeys.address1] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = call { dictionary[SerializationKeys.call] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = website { dictionary[SerializationKeys.website] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.address1 = aDecoder.decodeObject(forKey: SerializationKeys.address1) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.call = aDecoder.decodeObject(forKey: SerializationKeys.call) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.website = aDecoder.decodeObject(forKey: SerializationKeys.website) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(address1, forKey: SerializationKeys.address1)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(call, forKey: SerializationKeys.call)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(website, forKey: SerializationKeys.website)
  }

}
