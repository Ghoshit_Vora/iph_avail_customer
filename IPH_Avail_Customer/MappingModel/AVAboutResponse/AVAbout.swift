//
//  AVAbout.swift
//
//  Created on 21/05/18


import Foundation
import ObjectMapper

public final class AVAbout: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let twitterLink = "twitter_link"
    static let aboutImage = "about_image"
    static let linkdinLink = "linkdin_link"
    static let image = "image"
    static let text = "text"
    static let instaLink = "insta_link"
    static let googleLink = "google_link"
    static let facebookLink = "facebook_link"
  }

  // MARK: Properties
  public var twitterLink: String?
  public var aboutImage: String?
  public var linkdinLink: String?
  public var image: String?
  public var text: String?
  public var instaLink: String?
  public var googleLink: String?
  public var facebookLink: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    twitterLink <- map[SerializationKeys.twitterLink]
    aboutImage <- map[SerializationKeys.aboutImage]
    linkdinLink <- map[SerializationKeys.linkdinLink]
    image <- map[SerializationKeys.image]
    text <- map[SerializationKeys.text]
    instaLink <- map[SerializationKeys.instaLink]
    googleLink <- map[SerializationKeys.googleLink]
    facebookLink <- map[SerializationKeys.facebookLink]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = twitterLink { dictionary[SerializationKeys.twitterLink] = value }
    if let value = aboutImage { dictionary[SerializationKeys.aboutImage] = value }
    if let value = linkdinLink { dictionary[SerializationKeys.linkdinLink] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = text { dictionary[SerializationKeys.text] = value }
    if let value = instaLink { dictionary[SerializationKeys.instaLink] = value }
    if let value = googleLink { dictionary[SerializationKeys.googleLink] = value }
    if let value = facebookLink { dictionary[SerializationKeys.facebookLink] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.twitterLink = aDecoder.decodeObject(forKey: SerializationKeys.twitterLink) as? String
    self.aboutImage = aDecoder.decodeObject(forKey: SerializationKeys.aboutImage) as? String
    self.linkdinLink = aDecoder.decodeObject(forKey: SerializationKeys.linkdinLink) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.text = aDecoder.decodeObject(forKey: SerializationKeys.text) as? String
    self.instaLink = aDecoder.decodeObject(forKey: SerializationKeys.instaLink) as? String
    self.googleLink = aDecoder.decodeObject(forKey: SerializationKeys.googleLink) as? String
    self.facebookLink = aDecoder.decodeObject(forKey: SerializationKeys.facebookLink) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(twitterLink, forKey: SerializationKeys.twitterLink)
    aCoder.encode(aboutImage, forKey: SerializationKeys.aboutImage)
    aCoder.encode(linkdinLink, forKey: SerializationKeys.linkdinLink)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(text, forKey: SerializationKeys.text)
    aCoder.encode(instaLink, forKey: SerializationKeys.instaLink)
    aCoder.encode(googleLink, forKey: SerializationKeys.googleLink)
    aCoder.encode(facebookLink, forKey: SerializationKeys.facebookLink)
  }

}
