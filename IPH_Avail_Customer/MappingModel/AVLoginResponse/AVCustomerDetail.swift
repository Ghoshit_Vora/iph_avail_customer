//
//  AVCustomerDetail.swift
//
//  Created on 30/05/18


import Foundation
import ObjectMapper

public final class AVCustomerDetail: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iD = "ID"
    static let phone = "phone"
    static let name = "name"
    static let email = "email"
    static let type = "type"
    static let image = "image"
  }

  // MARK: Properties
  public var iD: String?
  public var phone: String?
  public var name: String?
  public var email: String?
  public var type: String?
  public var image: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    iD <- map[SerializationKeys.iD]
    phone <- map[SerializationKeys.phone]
    name <- map[SerializationKeys.name]
    email <- map[SerializationKeys.email]
    type <- map[SerializationKeys.type]
    image <- map[SerializationKeys.image]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.iD = aDecoder.decodeObject(forKey: SerializationKeys.iD) as? String
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(iD, forKey: SerializationKeys.iD)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(image, forKey: SerializationKeys.image)
  }

}
