//
//  AVLoginRequest.swift
//
//  Created on 30/05/18


import Foundation
import ObjectMapper

public final class AVLoginRequest: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let message = "message"
    static let msgcode = "msgcode"
    static let customerDetail = "customer_detail"
  }

  // MARK: Properties
  public var message: String?
  public var msgcode: String?
  public var customerDetail: [AVCustomerDetail]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    message <- map[SerializationKeys.message]
    msgcode <- map[SerializationKeys.msgcode]
    customerDetail <- map[SerializationKeys.customerDetail]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = msgcode { dictionary[SerializationKeys.msgcode] = value }
    if let value = customerDetail { dictionary[SerializationKeys.customerDetail] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.msgcode = aDecoder.decodeObject(forKey: SerializationKeys.msgcode) as? String
    self.customerDetail = aDecoder.decodeObject(forKey: SerializationKeys.customerDetail) as? [AVCustomerDetail]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(msgcode, forKey: SerializationKeys.msgcode)
    aCoder.encode(customerDetail, forKey: SerializationKeys.customerDetail)
  }

}
