//
//  AVOfferList.swift
//
//  Created on 11/07/18


import Foundation
import ObjectMapper

public final class AVOfferList: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let offerID = "offer_ID"
    static let name = "name"
    static let shreMsg = "shre_msg"
    static let image = "image"
    static let catButton = "cat_button"
    static let title = "title"
    static let message = "message"
    static let addedOn = "added_on"
    static let subcat = "subcat"
    static let buttonID = "buttonID"
  }

  // MARK: Properties
  public var offerID: String?
  public var name: String?
  public var shreMsg: String?
  public var image: String?
  public var catButton: String?
  public var title: String?
  public var message: String?
  public var addedOn: String?
  public var subcat: String?
  public var buttonID: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    offerID <- map[SerializationKeys.offerID]
    name <- map[SerializationKeys.name]
    shreMsg <- map[SerializationKeys.shreMsg]
    image <- map[SerializationKeys.image]
    catButton <- map[SerializationKeys.catButton]
    title <- map[SerializationKeys.title]
    message <- map[SerializationKeys.message]
    addedOn <- map[SerializationKeys.addedOn]
    subcat <- map[SerializationKeys.subcat]
    buttonID <- map[SerializationKeys.buttonID]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = offerID { dictionary[SerializationKeys.offerID] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = shreMsg { dictionary[SerializationKeys.shreMsg] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = catButton { dictionary[SerializationKeys.catButton] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = addedOn { dictionary[SerializationKeys.addedOn] = value }
    if let value = subcat { dictionary[SerializationKeys.subcat] = value }
    if let value = buttonID { dictionary[SerializationKeys.buttonID] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.offerID = aDecoder.decodeObject(forKey: SerializationKeys.offerID) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.shreMsg = aDecoder.decodeObject(forKey: SerializationKeys.shreMsg) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.catButton = aDecoder.decodeObject(forKey: SerializationKeys.catButton) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.addedOn = aDecoder.decodeObject(forKey: SerializationKeys.addedOn) as? String
    self.subcat = aDecoder.decodeObject(forKey: SerializationKeys.subcat) as? String
    self.buttonID = aDecoder.decodeObject(forKey: SerializationKeys.buttonID) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(offerID, forKey: SerializationKeys.offerID)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(shreMsg, forKey: SerializationKeys.shreMsg)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(catButton, forKey: SerializationKeys.catButton)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(addedOn, forKey: SerializationKeys.addedOn)
    aCoder.encode(subcat, forKey: SerializationKeys.subcat)
    aCoder.encode(buttonID, forKey: SerializationKeys.buttonID)
  }

}
