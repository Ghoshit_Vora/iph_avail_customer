//
//  AVNotificationRequest.swift
//
//  Created on 11/07/18


import Foundation
import ObjectMapper

public final class AVNotificationRequest: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let msgcode = "msgcode"
    static let offerList = "offer_list"
    static let message = "message"
    static let image = "image"
  }

  // MARK: Properties
  public var msgcode: String?
  public var offerList: [AVOfferList]?
  public var message: String?
  public var image: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    msgcode <- map[SerializationKeys.msgcode]
    offerList <- map[SerializationKeys.offerList]
    message <- map[SerializationKeys.message]
    image <- map[SerializationKeys.image]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = msgcode { dictionary[SerializationKeys.msgcode] = value }
    if let value = offerList { dictionary[SerializationKeys.offerList] = value.map { $0.dictionaryRepresentation() } }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.msgcode = aDecoder.decodeObject(forKey: SerializationKeys.msgcode) as? String
    self.offerList = aDecoder.decodeObject(forKey: SerializationKeys.offerList) as? [AVOfferList]
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(msgcode, forKey: SerializationKeys.msgcode)
    aCoder.encode(offerList, forKey: SerializationKeys.offerList)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(image, forKey: SerializationKeys.image)
  }

}
