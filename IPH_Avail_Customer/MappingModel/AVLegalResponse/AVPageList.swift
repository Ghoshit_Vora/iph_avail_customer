//
//  AVPageList.swift
//
//  Created on 05/07/18


import Foundation
import ObjectMapper

public final class AVPageList: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let title = "title"
    static let url = "url"
    static let sr = "sr"
  }

  // MARK: Properties
  public var title: String?
  public var url: String?
  public var sr: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    title <- map[SerializationKeys.title]
    url <- map[SerializationKeys.url]
    sr <- map[SerializationKeys.sr]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = url { dictionary[SerializationKeys.url] = value }
    if let value = sr { dictionary[SerializationKeys.sr] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.url = aDecoder.decodeObject(forKey: SerializationKeys.url) as? String
    self.sr = aDecoder.decodeObject(forKey: SerializationKeys.sr) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(url, forKey: SerializationKeys.url)
    aCoder.encode(sr, forKey: SerializationKeys.sr)
  }

}
