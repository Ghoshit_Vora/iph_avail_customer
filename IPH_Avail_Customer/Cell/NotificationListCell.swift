//
//  NotificationListCell.swift
//  IPH_Avail_Customer
//
//  Created on 11/07/18.


import UIKit

class NotificationListCell: UITableViewCell {

    @IBOutlet weak var imgOffer: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewContainer.layer.shadowColor = UIColor.lightGray.cgColor;
        viewContainer.layer.shadowOffset = CGSize(width: 0, height: 1)
        viewContainer.layer.shadowOpacity = 1
        viewContainer.layer.shadowRadius = 1.0
        viewContainer.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
