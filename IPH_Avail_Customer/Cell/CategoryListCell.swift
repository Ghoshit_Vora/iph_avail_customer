//
//  CategoryListCell.swift


import UIKit

class CategoryListCell: UITableViewCell {

    @IBOutlet weak var lblCategoryName: UILabel!
    
    @IBOutlet weak var imgCategory: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
