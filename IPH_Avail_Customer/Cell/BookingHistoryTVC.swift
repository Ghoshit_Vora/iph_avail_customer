//
//  BookingHistoryTVC.swift
//  IPH_Avail_Customer
//
//  Created on 17/07/18.


import UIKit

class BookingHistoryTVC: UITableViewCell {
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var lblBookingNumber: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblBookingStatus: UILabel!
    @IBOutlet weak var ivBookingProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        ivBookingProfile.layer.cornerRadius = ivBookingProfile.frame.width / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
