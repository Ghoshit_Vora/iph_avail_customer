//
//  PersonListCell.swift


import UIKit
import Cosmos

class PersonListCell: UITableViewCell {

    @IBOutlet weak var imgProfile               : UIImageView!
    @IBOutlet weak var lblName                  : UILabel!
    @IBOutlet weak var lblPrice                 : UILabel!
    @IBOutlet weak var ratingView               : CosmosView!
    @IBOutlet weak var lblDistance              : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
