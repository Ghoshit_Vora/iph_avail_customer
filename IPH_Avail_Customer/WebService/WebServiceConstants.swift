//
//  WebServiceConstants.swift

import Foundation

struct WebserviceURL {
    static var prod = ""
   // static var stage = "http://www.ambuler.in/avail/mapp/index.php?view="
    static var stage = "http://www.iavailnow.com/mapp/index.php?view="
}

enum WebserviceEndPoints: String {
    case about = "about"
    case category = "category"
    case dashboard = "dashboard"
    case login = "login"
    case register = "register"
    case otpVerify = "otp_verify"
    case vendors = "vendors"
    case resend_otp = "resend_otp"
    case help = "help"
    case contact = "contact"
    case legal = "pages"
    case subCategory = "sub_category"
    case referFriend = "share_msg"
    case notification = "offer_list"
    case bookingHistory = "bookings_list"
}
