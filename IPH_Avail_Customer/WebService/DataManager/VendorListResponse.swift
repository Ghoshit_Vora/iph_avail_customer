//
//  VendorListResponse.swift
//  IPH_Avail_Customer


import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
    //Get Category Details
    
    func getVendorData(catrgoryId : String, CustomerId : String,success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let vendorDataUrl = getUrl(.vendors) + "&page=list&custID=\(CustomerId)&catID=\(catrgoryId)&pagecode=0"
        
        NetworkManager.sharedManager.getJSON(vendorDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["msgcode"] == "0" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
}
