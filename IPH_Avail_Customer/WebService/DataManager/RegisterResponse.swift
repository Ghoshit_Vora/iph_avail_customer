//
//  LoginResponse.swift
//  IPH_Avail_Customer


import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
    //Get Register Details
    
    func setRegisterData(name : String,email : String, password : String, countryCode : String, mobile : String, success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let registerDataUrl = getUrl(.register) + "&sname=\(name)&semail=\(email)&spass=\(password)&sphone=\(mobile)&country_code=\(countryCode)&app_type=Iphone"
        
        NetworkManager.sharedManager.getJSON(registerDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["msgcode"] == "0" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
}

