//
//  DataManager.swift

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class DataManager: NSObject {
    
    static let sharedManager = DataManager()
    
    let baseUrl = WebserviceURL.stage
    
    
    //Get BaseUrl Methods
    
    func getUrl(_ endPoint: WebserviceEndPoints) -> String {
        
        return baseUrl + endPoint.rawValue
    }

}
