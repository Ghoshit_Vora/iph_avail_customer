//
//  LoginResponse.swift
//  IPH_Avail_Customer


import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
    //Get OTP Details
    
    func setOTPData(otp : String, custId : String,success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let otpDataUrl = getUrl(.otpVerify) + "&otp=\(otp)&custID=\(custId)"
        
        NetworkManager.sharedManager.getJSON(otpDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["msgcode"] == "0" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
    
    func setResendOTPData(custId : String,success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let otpDataUrl = getUrl(.login) + "&page=resend_otp&custID=\(custId)&app_type=Iphone"
        
        NetworkManager.sharedManager.getJSON(otpDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["msgcode"] == "0" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
}

