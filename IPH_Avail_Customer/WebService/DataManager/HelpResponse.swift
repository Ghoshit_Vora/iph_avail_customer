//
//  HelpResponse.swift
//  IPH_Avail_Customer
//
//  Created on 05/07/18.


import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
//    http://www.iavailnow.com/mapp/index.php?view=help&name=Mehul&email=test@gmail.com&contact_no=1234567890&message=test&app_type=Android
    
    //Get About Details
    
    func getHelpData(parameters: Parameters, success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let helpUrl = getUrl(.help)
        
        NetworkManager.sharedManager.getJSON(helpUrl, parameter: parameters, encoding: URLEncoding.default, header: nil, showHUD: .show, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            
            case .success(let value):
                
                let json = JSON(value)
                
                if json["message"] == "Success" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
                
            case .failure(let error):
                failure(error)
            }
            
        }) { (error) in
            failure(error)
        }
    }
}
