//
//  AboutResponse.swift
//  IPH_Avail_Customer

import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
    //Get Category Details
    
    func getCategoryData(custId : String,success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let categoryDataUrl = getUrl(.category) + "&custID=\(custId)"
        
        NetworkManager.sharedManager.getJSON(categoryDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["msgcode"] == "0" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
    
    func getSubCategoryData(custId : String, CatId : String,success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
       // http://www.iavailnow.com/mapp/index.php?view=sub_category&custID=MQ==&catID=
        let categoryDataUrl = getUrl(.subCategory) + "&custID=\(custId)&catID=\(CatId)"
        
        NetworkManager.sharedManager.getJSON(categoryDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["msgcode"] == "0" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
}

