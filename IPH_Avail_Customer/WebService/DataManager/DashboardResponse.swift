//
//  DashboardResponse.swift


import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
    //Get Dashboard Details
    
    func getDashboardData(custId : String, success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let aboutDataUrl = getUrl(.dashboard) + "&custID=\(custId)&token=123&deviceID=1478523&app_version=1.1&app_type=Iphone"
        
        NetworkManager.sharedManager.getJSON(aboutDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["message"] == "Success" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
}
