//
//  NotificationListResponse.swift
//  IPH_Avail_Customer
//
// Created on 11/07/18.


import Foundation
import Alamofire
import SwiftyJSON

extension DataManager {
    
    func getNotifcationData(success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let referUrl = getUrl(.notification)
        
        NetworkManager.sharedManager.getJSON(referUrl, encoding: URLEncoding.default, header: nil, showHUD: .show, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                
                if json["message"] == "Success" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
                
            case .failure(let error):
                failure(error)
            }
            
        }) { (error) in
            failure(error)
        }
    }
}
