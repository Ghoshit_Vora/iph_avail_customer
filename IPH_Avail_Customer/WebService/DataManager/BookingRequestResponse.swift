//
//  NotificationListResponse.swift
//  IPH_Avail_Customer
//
// Created on 11/07/18.


import Foundation
import Alamofire
import SwiftyJSON

extension DataManager {
    
    func getBookingHistoryData(custId : String,pageNo:String,success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        //http://www.iavailnow.com/mapp/index.php?view=bookings_list&page=list&custID=MQ==&pagecode=0
        let referUrl = getUrl(.bookingHistory) + "&page=list&custID=\(custId)&pagecode=\(pageNo)"
        
        NetworkManager.sharedManager.getJSON(referUrl, encoding: URLEncoding.default, header: nil, showHUD: .show, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                
                if json["message"] == "Success" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
                
            case .failure(let error):
                failure(error)
            }
            
        }) { (error) in
            failure(error)
        }
    }
}

