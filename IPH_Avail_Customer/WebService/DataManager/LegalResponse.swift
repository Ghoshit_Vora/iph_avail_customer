//
//  LegalResponse.swift
//  IPH_Avail_Customer
//
//  Created on 05/07/18.


import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
    func getLegalData(success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let helpUrl = getUrl(.legal)
        
        NetworkManager.sharedManager.getJSON(helpUrl, encoding: URLEncoding.default, header: nil, showHUD: .show, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                
                if json["message"] == "Success" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
                
            case .failure(let error):
                failure(error)
            }
            
        }) { (error) in
            failure(error)
        }
    }
    
}
