//
//  AboutResponse.swift
//  IPH_Avail_Customer


import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
    
    //Get About Details
    
    func getAboutData(success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {

        let aboutDataUrl = getUrl(.about)

        NetworkManager.sharedManager.getJSON(aboutDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["message"] == "Success" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
}
