//
//  LoginResponse.swift
//  IPH_Avail_Customer


import Foundation
import SwiftyJSON
import Alamofire

extension DataManager {
    
    //Set Login Details
    
    func setLoginData(email : String, password : String, success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let loginDataUrl = getUrl(.login) + "&page=login&user=\(email)&pass=\(password)&app_type=Iphone"
        
        NetworkManager.sharedManager.getJSON(loginDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["msgcode"] == "0" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
    
    func setForgotData(email : String,success : @escaping (Any? , Any?) -> Void, failure: @escaping (Error) -> Void)  {
        
        let forGotDataUrl = getUrl(.login) + "&page=forgot_password&user=\(email)&app_type=Iphone"
        
        NetworkManager.sharedManager.getJSON(forGotDataUrl, message: LoadingMsg.lodingMsg, success: { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["msgcode"] == "0" {
                    success(value, nil)
                } else {
                    success(nil, value)
                }
            case .failure(let error):
                
                failure(error)
            }
        }) { (error) in
            
            failure(error)
        }
    }
    
    
    
    
}
