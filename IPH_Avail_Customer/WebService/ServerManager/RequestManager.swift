//
//  RequestManager.swift

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

typealias ImageParameter = [[String: Data]]

class RequestManager: SessionManager {

    static let sharedManager = RequestManager()

    func responseRequest(_ url: String, method: Alamofire.HTTPMethod, parameter: Parameters? = nil, encoding: ParameterEncoding, header: HTTPHeaders? = nil, completionHandler: @escaping (DefaultDataResponse) -> Void) -> Void {

        self.request(url, method: method, parameters: parameter, encoding: encoding, headers: header).response { response in
            completionHandler(response)
        }

    }

    func stringRequest(_ url: String, method: Alamofire.HTTPMethod, parameter: Parameters? = nil, encoding: ParameterEncoding, header: HTTPHeaders? = nil, completionHandler: @escaping (DataResponse<String>) -> Void) -> Void {

        self.request(url, method: method, parameters: parameter, encoding: encoding, headers: header).responseString { response in
            completionHandler(response)
        }

    }

    func JSONRequest(_ url: String, method: Alamofire.HTTPMethod, parameter: Parameters? = nil, encoding: ParameterEncoding, header: HTTPHeaders? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void) -> Void {

        self.request(url, method: method, parameters: parameter, encoding: encoding, headers: header).responseJSON { response in
            completionHandler(response)
        }

    }

    func objectResponseRequest<T: BaseMappable>(_ url: String, method: Alamofire.HTTPMethod, parameter: Parameters? = nil, encoding: ParameterEncoding, header: HTTPHeaders? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Void {

        self.request(url, method: method, parameters: parameter, encoding: encoding, headers: header).responseObject { (response: DataResponse<T>) in
            completionHandler(response as DataResponse<T>)
        }

    }

    func arrayResponseRequest<T: BaseMappable>(_ url: String, method: Alamofire.HTTPMethod, parameter: Parameters? = nil, encoding: ParameterEncoding, header: HTTPHeaders? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Void {

        self.request(url, method: method, parameters: parameter, encoding: encoding, headers: header).responseArray { (response: DataResponse<[T]>) in
            completionHandler(response as DataResponse<[T]>)
        }
    }

    func uploadImageJSON(_ endPoint: String, method: Alamofire.HTTPMethod, images: ImageParameter, paramater: Parameters? = nil, encoding: ParameterEncoding, header: HTTPHeaders? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void) -> Void {
        self.upload(multipartFormData: { multipartFormData in

            for (key, value) in paramater! {
                if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: key)
                }
            }
            for (key, value) in paramater! {
                multipartFormData.append(value as! Data, withName: key)
            }
        }, to: endPoint, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    completionHandler(response)
                }
                break
            case .failure(let encodingError):
                completionHandler(encodingError as! DataResponse<Any>)
                break
            }
        })
    }
}
