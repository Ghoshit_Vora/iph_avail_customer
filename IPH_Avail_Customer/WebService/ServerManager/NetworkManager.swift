//
//  NetworkManager.swift

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

enum HUDFlag: Int {
    case show = 1
    case hide = 0
}

class NetworkManager {

    static let sharedManager = NetworkManager()

    // ----------------------------------------------------------------

    // MARK: Get Request Method

    // ----------------------------------------------------------------

    func getResponse(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DefaultDataResponse) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.responseRequest(url, method: .get, parameter: parameter, encoding: encoding, header: header) { response in

                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {

            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)

        }

    }

    func getString(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<String>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.stringRequest(url, method: .get, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func getJSON(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<Any>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.JSONRequest(url, method: .get, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func getObject<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<T>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.objectResponseRequest(url, method: .get, encoding: encoding, completionHandler: { (response: DataResponse<T>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<T>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }

    }

    func getArray<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<[T]>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.arrayResponseRequest(url, method: .get, encoding: encoding, completionHandler: { (response: DataResponse<[T]>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<[T]>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    // ----------------------------------------------------------------

    // MARK: Post Request Method

    // ----------------------------------------------------------------

    func postResponse(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DefaultDataResponse) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.responseRequest(url, method: .post, parameter: parameter, encoding: encoding, header: header) { response in

                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {

            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)

        }

    }

    func postString(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<String>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() == true {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.stringRequest(url, method: .post, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func postJSON(_ url: String, parameter: Parameters, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<Any>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() == true {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.JSONRequest(url, method: .post, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func postObject<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<T>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() == true {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.objectResponseRequest(url, method: .post, encoding: encoding, completionHandler: { (response: DataResponse<T>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<T>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }

    }

    func postArray<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<[T]>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.arrayResponseRequest(url, method: .post, encoding: encoding, completionHandler: { (response: DataResponse<[T]>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<[T]>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    // ----------------------------------------------------------------

    // MARK: Put Request Method

    // ----------------------------------------------------------------

    func putResponse(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DefaultDataResponse) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.responseRequest(url, method: .put, parameter: parameter, encoding: encoding, header: header) { response in

                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {

            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)

        }

    }

    func putString(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<String>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.stringRequest(url, method: .put, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func putJSON(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<Any>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.JSONRequest(url, method: .put, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func putObject<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<T>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.objectResponseRequest(url, method: .put, encoding: encoding, completionHandler: { (response: DataResponse<T>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<T>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }

    }

    func putArray<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<[T]>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.arrayResponseRequest(url, method: .put, encoding: encoding, completionHandler: { (response: DataResponse<[T]>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<[T]>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    // ----------------------------------------------------------------

    // MARK: Put Request Method

    // ----------------------------------------------------------------

    func deleteResponse(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DefaultDataResponse) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.responseRequest(url, method: .delete, parameter: parameter, encoding: encoding, header: header) { response in

                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {

            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)

        }

    }

    func deleteString(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<String>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.stringRequest(url, method: .delete, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func deleteJSON(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<Any>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.JSONRequest(url, method: .delete, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func deleteObject<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<T>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.objectResponseRequest(url, method: .delete, encoding: encoding, completionHandler: { (response: DataResponse<T>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<T>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }

    }

    func deleteArray<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<[T]>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.arrayResponseRequest(url, method: .delete, encoding: encoding, completionHandler: { (response: DataResponse<[T]>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<[T]>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    // ----------------------------------------------------------------

    // MARK: Patch Request Method

    // ----------------------------------------------------------------

    func patchResponse(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DefaultDataResponse) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.responseRequest(url, method: .patch, parameter: parameter, encoding: encoding, header: header) { response in

                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {

            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)

        }

    }

    func patchString(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<String>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.stringRequest(url, method: .patch, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func patchJSON(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<Any>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.JSONRequest(url, method: .patch, parameter: parameter, encoding: encoding, header: header) { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            }
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    func patchObject<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<T>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.objectResponseRequest(url, method: .patch, encoding: encoding, completionHandler: { (response: DataResponse<T>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<T>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }

    }

    func patchArray<T: BaseMappable>(_ url: String, parameter: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<[T]>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)

            RequestManager.sharedManager.arrayResponseRequest(url, method: .patch, encoding: encoding, completionHandler: { (response: DataResponse<[T]>) in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response as DataResponse<[T]>)
            })
        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }
    }

    // ----------------------------------------------------------------

    // MARK: Upload Request Method

    // ----------------------------------------------------------------

    func postImage(_ url: String, parameter: Parameters? = nil, images: ImageParameter, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<Any>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)
            RequestManager.sharedManager.uploadImageJSON(url, method: .post, images: images, paramater: parameter, encoding: encoding, header: header, completionHandler: { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            })

        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }

    }

    func putImage(_ url: String, parameter: Parameters? = nil, images: ImageParameter, encoding: ParameterEncoding = URLEncoding.default, header: HTTPHeaders? = nil, showHUD: HUDFlag = .show, message: String, success: @escaping (DataResponse<Any>) -> Void, failure: @escaping (Error) -> Void) -> Void {

        if AppDelegate.sharedInstance.isConnectedToNetwork() {

            showHideHud(showHUD: showHUD, message: message, show: .show)
            RequestManager.sharedManager.uploadImageJSON(url, method: .post, images: images, paramater: parameter, encoding: encoding, header: header, completionHandler: { response in
                self.showHideHud(showHUD: showHUD, message: message, show: .hide)
                success(response)
            })

        } else {
            self.showHideHud(showHUD: showHUD, message: message, show: .hide)
            failure(ErrorManager.noInternetConnectionError)
        }

    }

    // MARK: Show / Hide Hud

    internal func showHideHud(showHUD: HUDFlag, message: String, show: HUDFlag) {

        if show.rawValue == 1 {

            if showHUD.rawValue == 1 {
                AppDelegate.sharedInstance.startLoadingIndicator(message)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            if showHUD.rawValue == 1 {
                AppDelegate.sharedInstance.stopLoadingIndicator()
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }

        }
    }
}
